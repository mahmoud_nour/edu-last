<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts',function( Blueprint $table){
            $table->increments('id');
            $table->integer('admin_id');
            $table->string('title')->unique();
            $table->text('body');
            $table->enum('department',['electronics_and_communication','electrical_power','civil','architecture','bio_medical','chemical']);
            $table->enum('edu_year',['Preparatory_Year','First_Year','Second_Year','Third_Year','Fourth_Year']);
            $table->string('subject');
            $table->string('term');
            $table->string('edited_by')->nullable();
            $table->dateTime('edited_at')->nullable();
            $table->string('imagePath')->nullable();
            $table->string('pdfPath')->nullable();
            $table->enum('category',['section','lecture','midterm','final']);
            $table->boolean('approved')->default(0);
            $table->string('approved_by')->nullable();
            $table->date('approved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
