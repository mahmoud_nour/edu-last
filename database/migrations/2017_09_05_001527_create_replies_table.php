<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // id 
        //  ,  ,  ,  ,  , body, , approted_at, approved_by
        Schema::create('replies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('comment_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('edited_by')->unsigned()->nullable();
            $table->integer('approved_by')->unsigned()->nullable();

            $table->text('body');
            $table->text('edited_body')->nullable();
            $table->boolean('approved')->default(0);
            $table->date('approved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replies');
    }
}
