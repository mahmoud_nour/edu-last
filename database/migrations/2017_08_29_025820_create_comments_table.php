<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->boolean('approved')->default(0);
            $table->date('approved_at')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('edited_by')->nullable();
            $table->string('edited_body')->nullable();
            $table->text('body');
            $table->integer('post_id')->unsigned();
            $table->timestamps();
           
            
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('comments');
    }
}
