<?php

use Illuminate\Database\Seeder;

class HintSeeding extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      	 DB::table('hints')->insert(array(
            array('branches'=>NULL,'term'=>'first_term','year'=>'Preparatory_Year','subjects' => 'math1,physics1,mechanics1'),
            array('branches'=>'electronics_and_communication,electrical_power,civil,architecture,biomedical,chemistry','term'=>'second_term','year'=>'Preparatory_Year','subjects' => 'math2,physics2,mechanics2'),
            array('branches'=>'electronics_and_communication','term'=>'first_term','year'=>'First_Year','subjects' => 'math3,physics3'),
            array('branches'=>'electronics_and_communication','term'=>'second_term','year'=>'First_Year','subjects' => 'math4,physics4')
        ));
    }
}
