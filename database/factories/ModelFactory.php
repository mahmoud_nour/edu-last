<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'username' => $faker->unique()->name,
        'sec_question' => 'Where are You From',
        'sec_answer' => $faker->country,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'location' => $faker->country,
        'dob' => $faker->dateTime,
        'department' => 'Communication',
        'year' => 'First_Year',
        
    ];
});

