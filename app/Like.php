<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
 Relation::morphMap([
   'Post' => 'App\Post',
  'Comment' => 'App\Comment',
  'Reply' => 'App\Reply'
]);

class Like extends Model
{
   // public $timestamps = false;
  protected $fillable = ['like_status'];
  protected $casts = [
    'like_status' => 'boolean'
  ];
   public function likable(){
      return $this->morphTo();
   }
   public function user(){
    return $this->belongsTo(User::class,'user_id');
  }
  public static function status($type,$id){
    return static::where(['user_id' => \Sentinel::getUser()->id , 'likable_type' => $type , 'likable_id' => $id])->first()->like_status ?? NULL;
  }
  public static function handle($type,$id){
    return  static::where(['user_id' => \Sentinel::getUser()->id , 'likable_type' => $type , 'likable_id' => $id])->first();
  }
  public  function counter($item, int $value) : int {
    return static::where(['likable_id' => $item->id, 'likable_type' => class_basename($item),'like_status'  => $value])->count();
  }

}