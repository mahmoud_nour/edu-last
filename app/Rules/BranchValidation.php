<?php

namespace App\Rules;
use \App\Hint;
use Illuminate\Contracts\Validation\Rule;

class BranchValidation implements Rule
{
    private $hint;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($term,$year)
    {
        $this->hint = Hint::where(['term' => $term, 'year' => $year])->first();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

     
            if(!$this->hint){
                return false;
            }
            $data = request()->validate([
                'department' => 'in:' . $this->hint->branches. '',

            ]);
            if(is_array($data)){
                return true;
             }
            return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This branch does not exist for this term.';
    }
}
