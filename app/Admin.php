<?php

namespace App;
use Activation;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Notifications\Notifiable;
use Sentinel;
use \Carbon\Carbon;

class Admin extends EloquentUser {
	use Notifiable;
	protected $table = 'users';
	public function posts() {
		return $this->hasMany(Post::class);
	}
	
	public function age()
	{
		return Carbon::parse($this->attributes['dob'])->age;
	}
	public function tags() {
		return $this->hasMany(Tag::class);
	}
	static function listOnlineUsers() {
		$users = static::pluck('id')->all();
		foreach ($users as $user) {

			if (Activation::completed(static::find($user))) {
				if (count(Sentinel::findById($user)->persistences) > 0) {
					$online_users[] = Sentinel::findByPersistenceCode(Sentinel::findById($user)->persistences->first()->code);
				}

			}
			continue;
		}
		return $online_users ?? NULL;
	}

	public static function upgradeUser(int $id, $permissions) {
		$user = Sentinel::findById($id);
		if (!$user) {
			return false;
		}
		if ($user->hasAccess('admin.*')) {
			return false;
		}

        if (!$permissions) {
            return false;
        }

		if (is_array($permissions)) {
			foreach ($permissions as $permission => $value) {

				$user->updatePermission($permission, $value, true)->save();
			}
			return true;
		} elseif(preg_match('/^[a-zA-Z.]*$/',$permissions)){
			$user->addPermission($permissions)->save();
			return true;
		}
		return false;
	}
	static function downgradeUser($id, $permissions) {
		$user = Sentinel::findById($id);
		if (!$user) {
			return false;
		}
		if ($user->hasAccess('admin.*')) {
			return false;
		}
        if (!$permissions) {
            return false;
        }
		if (is_array($permissions)) {
			foreach ($permissions as $permission => $value) {
				$user->updatePermission($permission, $value, true)->save();
			}
			return true;
		} elseif(is_string($permissions)) {
			$user->updatePermission($permissions, false, true)->save();
			return true;
		}
		return false;
	}
	public static function approve($id, $type) {

		$post = $type::find($id)->whereApproved(0)->first();
		if ($post) {
			$post->approved = 1;
			$post->approved_by = Sentinel::getUser()->username;
			$post->approved_at = date('Y-m-d H:i:s');
			$post->save();
			return true;
		}
		return false;
	}

	public static function listUsers() {
		foreach (\App\Admin::pluck('id') as $value) {
			$user = \Sentinel::findById($value);
			if (!$user) {
				return false;
			}
			if (\Activation::completed($user)) {
				if (!$user->hasAnyAccess(['admin.*'])) {
					$users[] = $user;
				}
			}
			continue;
		}
		return $users ?? NULL;
	}
}
