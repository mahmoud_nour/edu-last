<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = 'replies';
    protected $fillable = ['body','edited_body','approved','approved_by','approved_at'];
    public function getCreatedAtAttribute($value){

        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value)->diffForHumans();
    }
    public function getUpdatedAtAttribute($value){

        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value)->diffForHumans();
    }
    public function comment(){
        return $this->belongsTo(Comment::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
      
    public function editor(){
        return $this->belongsTo(User::class,'edited_by','id');
    }
    public function post(){
        return $this->belongsTo(Post::class);
    }
     public function likes(){
      return$this->morphMany(Like::class,'likable');
   }
}
