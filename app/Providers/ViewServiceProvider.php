<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
          view()->composer('layouts.navbar',function($view){
                $view->with('menus',\App\Hint::menuElements());
            });
            view()->composer('layouts.sidebar',function($view){
                if(!in_array('guest',request()->route()->middleware())){
                    $view->with('archives',\App\Post::archives());
                    $view->with('tags',\App\Tag::PopularTags());
                    $view->with('posts',\App\Post::recentPosts());
                    $view->with('popularPosts',\App\Post::popularPosts());
                }
                if(\Route::currentRouteName() === 'register' || \Route::currentRouteName() === 'login'){
                    $view->with('lr_tips', ['Register For Free','Get All Materials you need','Download everything freely','Get Notification,whenever something is released']);
                }
                if(\Route::currentRouteName() === 'posts.category.index'){
                   
                    $view->with('categories',\App\Post::listBySubject(request()->route('subject')));
                }
                if (\Sentinel::check() && \Sentinel::getUser()->hasAccess('admin.*')) {
                    $view->with(['online_users' => \App\Admin::listOnlineUsers()]);
                }

            });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
