<?php

namespace App;
use \Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\EloquentUser;
class User extends EloquentUser
{
    
    use Notifiable;
    protected $fillable = ['sec_answer','sec_question','username','first_name','last_name','email','password','location','dob','department','edu_year'];
    public function getRouteKeyName(){
        return 'username';
    }
   
    public function rates(){
        return $this->hasMany(Rate::class,'user_rated');
    }
    
    public function age(){
        return Carbon::parse($this->attributes['dob'])->age;
         //         return Carbon::parse($this->attributes['dob'])->diff(Carbon::now())->format('%y years , %m months And  %d days');
    }
    
   public function getSecQuestionAttribute($value){
        return $this->attributes['sec_question'] = ucfirst(str_replace('_',' ', $value));
    }
    public function comments(){
    	return $this->hasMany(Comment::class);
    }
    public function Tags(){
    	return $this->hasMany(Tag::class,'admin_id');
    }
    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function userTags($username){
	    // Equal to dd(DB::table('tags')->where('user_id',1)->get());
    	return static::find($username)->tags;
    }
    public function userComments($username){
    	return static::find($username)->comments;
    }
    public function listUserWhoCreatedTags(){
    	return static::has('tags')->get();
    }
    public static function PopularAdmins()
    {
       return  static::join('posts', 'posts.user_id', '=', 'users.id')
        ->select(\DB::table('posts')->raw('count(posts.*) as posts_count'))
        ->orderBy('posts_count', 'desc')
        ->get();
    }
    public static function PeopleCommentedOnAPost($post){
        return $post->comments()->where('user_id','!=',$post->admin->id)->distinct()->get()->unique('user_id');
    }
    public function phoneNumber()
    {
        return $this->hasOne(PhoneNumber::class);
    }
    public function hasTWoFactorAuthenticationEnabled(){
        return $this->two_factor_type !== 'off';
    }
    public function hasSmsTwoFactorAuthenticationEnabled()
    {
        return $this->two_factor_type === 'sms';
    }
    public function hasTwoFactorType($type){
        return $this->factor_type === $type;
    }
    public function hasDiallingCode($diallingCodeId){
        if($this->hasPhoneNumber() === false){
            return false;
        }
        return $this->phoneNumber->diallingCode->id === $diallingCodeId;
    }
    public function registeredForTwoFactorAuthentication(){
        return $this->authy_id !== null;
    }
    public function updatePhoneNumber($phoneNumber,$phoneNumberDiallingCode){
        $this->phoneNumber()->delete();
        if(!$phoneNumber){
            return;
        }
        return $this->phoneNumber()->create([
            'phone_number' =>$phoneNumber ,
            'dialling_code_id' => $phoneNumberDiallingCode
        ]);
    }
    public function hasPhoneNumber(){
        return $this->phoneNumber !== null;
    }
    public function getPhoneNumber(){
        if($this->hasPhoneNumber() === false){
            return false;
        }
        return $this->phoneNumber->phone_number;

    }
    
}
