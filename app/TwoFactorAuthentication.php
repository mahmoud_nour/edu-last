<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class TwoFactorAuthentication extends Model
{
    protected $table = 'users';
    public function phoneNumber()
    {
        return $this->hasOne(PhoneNumber::class);
    }
    public function hasTWoFactorAuthenticationEnabled(){
        return $this->two_factor_type !== 'off';
    }
    public function hasSmsTwoFactorAuthenticationEnabled()
    {
        return $this->two_factor_type === 'sms';
    }
    public function hasTwoFactorType($type){
        return $this->factor_type === $type;
    }
    public function hasDiallingCode($diallingCodeId){
        if($this->hasPhoneNumber() === false){
            return false;
        }
        return $this->phoneNumber->diallingCode->id === $diallingCodeId;
    }
    public function registeredForTwoFactorAuthentication(){
        return $this->authy_id !== null;
    }
    public function updatePhoeNumber($phoneNumber,$phoneNumberDiallingCode){
        $this->phoneNumber()->delete();
        if(!$phoneNumber){
            return;
        }
        return $this->phoneNumber()->create([
            'phone_number' =>$phoneNumber ,
            'dialling_code_id' => $phoneNumberDiallingCode
        ]);
    }
    public function hasPhoneNumber(){
        return $this->phoneNumber !== null;
    }
    public function getPhoneNumber(){
        if($this->hasPhoneNumber() === false){
            return false;
        }
        return $this->phoneNumber->phone_number;

    }
}
