<?php

namespace App;
use Sentinel;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Post extends Model
{
    protected $fillable = ['title','body','edited_by','updated_at','department','subject','category','created_at','admin_id','term','approved','approved_at','approved_by'];
    protected $dates = [
        'approved_at','created_at','updated_at'
    ];
    public function setTitleAttribute($value){

        $this->attributes['title'] = str_slug($value);
    }
    public function hints()
    {
        return $this->hasMany(Hint::class,'year','edu_year');
    }
    public function RecentPostsDependingOnBranch($year,$department = null)
    {
        if ($year == NULL && $department == NULL) {
                $posts = [];
                $i = 0;
            foreach (static::where(array('approved' => 1))->pluck('subject')->toArray()  as $subject) {
                if ($i == 3) {
                    break;
                }
                if (!array_key_exists($subject,$posts)) {
                    $posts[$subject][] = static::where(array('approved' => 1,'subject' => $subject))->orderByDesc('created_at')->take(3)->get();
                    $i++;
                }
            }
        }
        $recentPosts = $this->hints()->where(['year'  => $year, 'branches' => $department])->orWhere(['year'  => $year])->get(['subjects','term']);
        foreach ($recentPosts as $post) {
            foreach (explode(',',$post->subjects) as $subject) {
                if (static::whereSubject($subject)->exists()) {
                    
                    $posts[$subject][] = $this->recentPosts($subject,$post->term);
                }
            }
        }
        return $posts ?? NULL;
    }
    public function likes(){
      return $this->morphMany(Like::class,'likable');
   }

    public static function listBySubject($subject,$category = null){
        if(is_null($category)){
           return  $post = Post::where(['subject' => $subject, 'approved' => 1])->orderByDesc('created_at')->get();
        }
        return $post = Post::where(['subject' => $subject, 'approved' => 1,'category' => $category])->orderByDesc('created_at')->get();
    }
    public static function archives()
    {
        return static::selectRaw('year(created_at) year, monthname(created_at) month , count(*) published')->
        groupBy('year','month')->
        orderByRaw('min(created_at) desc')->
        get()->toArray();
    }

    public function getRouteKeyName(){
        return 'title';
    }
    public  function tags(){
        return $this->belongsToMany('\App\Tag');
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function replies(){
        return $this->hasMany(Reply::class);
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
    public function getCreatedAtAttribute($value){
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value)->diffForHumans(); 
    }
    public static function listUnApproved(){
        return static::whereApproved(0)->orderBy('created_at','desc')->paginate(10);
    }
    public  static function recentPosts($subject = null,$term = null){
        if($subject == null){
            return static::whereApproved(1)->orderByDesc('created_at')->take(10)->get();
            
        }
        return static::where(['approved' => 1 , 'subject' => $subject,'term' => $term])->orderByDesc('created_at')->take(5)->get();

    }
    public static function listApproved(){
        return static::whereApproved(1)->orderBy('created_at','desc');
    }
    
    public  static function getByTag($name){
        return self::with('tags')->whereName($name);
    }
    
    public static function PopularPosts(){
        
        return Post::join('comments', 'comments.post_id', '=', 'posts.id')
            ->groupBy(['posts.id','posts.title'])
            ->where('posts.approved',1)->get(['posts.id', 'posts.title', \DB::raw('count(posts.id) as post_count')])
            ->sortByDesc('post_count')->take(5);
       
    }
    public function scopeFilter($query,$month = null,$year = null){
       
        if(isset($month)){
            $query->whereMonth('created_at',Carbon::parse($month)->month);
        }
        if(isset($year)){
            $query->whereYear('created_at',$year);
        }
    }
    
   

}

?>
