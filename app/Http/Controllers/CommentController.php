<?php

namespace App\Http\Controllers;

use App\Comment;
use \Sentinel;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct(){
        $this->middleware('user')->only(['show','edit','delete']);
        $this->middleware('admin')->only('index');
    }
    public function index()
    {
        return view('comments.index')->with('comments',Comment::all());
    }

 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,\App\Post $post)
    {
        $this->validate($request,[
            'comment' => 'required|max:500|min:3'
        ]);
        $comment = new Comment;
        $comment->body = request('comment');
        $comment->user_id = Sentinel::getUser()->id;
        $comment->updated_at = null;
        $comment->post()->associate($post);
        $comment->save();
        //$post = $post->comments()->where('user_id', '!=', Sentinel::getUser()->id)->pluck('user_id','user_id')->toArray();
       // dd($post);
        // \Illuminate\Support\Facades\Notification::send(\App\User::PeopleCommentedOnAPost($post), new \App\Notifications\CommentNotification($post));
       $post->admin->notify(new \App\Notifications\CommentNotification($post,\App\User::PeopleCommentedOnAPost($post)));
            
       
            
        
        return back()->with('success','Commented Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        
        return view('posts.show')->with(['post' => $comment->post,'editor' => Comment::Editor() ]);
    }

   
    public function edit(Comment $comment, \App\Post $post){
        return view('posts.show')->with(['comment' => $comment,'post' => $post , 'like' => new \App\Like]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Comment $comment)
    {
        $this->validate(request(),[
            'comment' => 'required|max:500|min:3'
        ]);
      
        $comment->edited_body = request('comment');
        $comment->edited_by = Sentinel::getUser()->id;
        $comment->updated_at = date('Y-m-d H:i:s'); 
        $comment->save();
        return redirect()->route('posts.show',$comment->post->title)->with('success','Comment Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back()->with('success','Comment Deleted !');

    }
    public function approveComment(Comment $comment)
    {
        return (\App\Admin::approve($comment->id,'\App\Comment')) ? back()->with('success' ,'Comment Approved') : back()->with('error','Failed to Approve Comment');
    }
}
