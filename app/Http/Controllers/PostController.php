<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Post;
use Sentinel;
use Carbon\Carbon;

class PostController extends Controller
{

   private $uploads = ['images'=>'imagePath','pdf' => 'pdfPath'];
  
    public static function listBySubject($subject,$category = null){
        $validator = \Validator::make([
            'subject' => $subject,
            'category' => $category
        ],[
            'subject' => 'required|min:5|max:15|exists:posts,subject',
            'category' => 'nullable|in:section,midterm,final,lecture'
        ]);
        if($validator->fails()){
            return redirect()->home()->withErrors($validator);
        }
        $posts = Post::listBySubject($subject,$category);
        if($posts->count()){
            return view('posts.index',compact('posts'));
        }
        return redirect()->route('posts.index')->with('error','No such subject nor category');

    }
   
    public function unApproved(){
        $posts = Post::listUnApproved();
        return view('posts.index')->with(['posts' => $posts]);
    }
    public function getByArchive(){
        $posts = Post::listApproved()->filter(request()->route('month'),request()->route('year'))->get();
        return view('posts.index',compact('posts'));
    }
    public function index()
    {
        $posts = Post::listApproved()->get();    
        return view('posts.index')->with('posts',$posts);
    }

 
    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {

      
        $data = request()->validate([
            'title' => 'required|string|unique:posts,title|min:3|max:32',
            'body' => [
                'required','string','regex:/^[a-zA-Z0-9-_. ]*$/','min:6','max:300'
            ],
            'imagePath' => 'image|nullable|max:1999',
            'pdfPath' => 'max:10000|nullable',
            'edu_year' => 'required|in:Preparatory_Year,First_Year,Second_Year,Third_Year,Fourth_Year',
            'term' => 'required|string|in:first_term,second_term',
            'category' => 'required|string|in:section,mid-term,final,lecture',
            'subject' => new \App\Rules\SubjectValidation(request('term'),request('edu_year')),
            'department' =>  new \App\Rules\DepartmentValidation(request('term'),request('edu_year')),
            'tags'  => [
                'required','regex:/^[a-zA-Z0-9-_. ]*$/','min:3','max:32','string'
            ],
        ]);
        $now = \Carbon\Carbon::now();
       

       	$data['admin_id'] = Sentinel::getUser()->id;
       	$data['created_at'] = date('Y-m-d H:i:s');
       	$data['updated_at'] = null;
        if (Sentinel::getUser()->hasAccess('admin.create')) {
            $data['approved'] = 1;
            $data['approved_by'] = Sentinel::getUser()->username;
            $data['approved_at'] = date('Y-m-d H:i:s');
        }
        if(\App\Tag::assignTags(request('tags'))){
            $post = Post::create($data);
        }
        if(is_array(\Session::get('tags'))){
            foreach (\Session::get('tags') as $tag) {
                $post->tags()->attach($tag);
            }
        }else{
            $post->tags()->attach(\Session::get('tags'));
        }
         
        \Session::forget('tags');
        foreach ($this->uploads as $key => $upload) {
            if(request()->hasFile($upload)){
                $file_with_ext = request()->file($upload)->getClientOriginalName();
                $file_ext = request()->file($upload)->getClientOriginalExtension();
                $file_name_new = sha1(str_random(40). time() ) . '.' . $file_ext;
                $path = request()->file($upload)->move(public_path() ."/$key/",$file_name_new);
                // $path->getPath();
                Post::whereTitle(request('title'))->update([$upload => $file_name_new]);
            }
            continue;
        }
        return redirect()->route('posts.index')->with('success','Post Created');
    }

    
    public function show(Post $post) 
    {
        if($post->approved === 1){
            $like = new \App\Like;
        
            return view('posts.show')->with(['post'=> $post , 'like' => $like]);

        }
        return redirect()->route('posts.index')->with('error','Post Is not approved yet to be shown');
        
      
    }

    public function edit(Post $post)
    {
        if($post->approved === 1){
            $like = new \App\Like;
        
            if(Sentinel::getUser()->id != $post->admin_id && Sentinel::getUser()->hasAccess('moderator.edit')){
                return redirect()->route('posts.index')->with('error','Unauthorized Page');
            }
            return view('posts.edit')->with(['post'=> $post , 'like' => $like]);

        }
        return redirect()->route('posts.index')->with('error','Post Is not approved yet to be shown');
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  request()
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post){
      
    	$data = request()->validate([
            'title' => "required|string|unique:posts,title,$post->id|min:3|max:32",
            'body' => [
                'required','string','regex:/^[a-zA-Z0-9-_.\'\" ]*$/','min:6','max:300'
            ],
            'imagePath' => 'image|nullable|max:1999',
            'pdfPath' => 'max:10000|nullable',
            'edu_year' => 'required|in:Preparatory_Year,First_Year,Second_Year,Third_Year,Fourth_Year',
            'term' => 'required|in:first_term,second_term',
            'category' => 'required|string|in:section,mid-term,final,lecture',
            'subject' => new \App\Rules\SubjectValidation(request('term'),request('edu_year')),
            'department' => new \App\Rules\DepartmentValidation(request('term'),request('edu_year')),
            'tags'  => [
                'nullable','regex:/^[a-zA-Z0-9-_. ]*$/','min:3','max:32','string'
            ],
        ]);
       	$data['edited_by'] = Sentinel::getUser()->id;
       	$data['updated_at'] = date('Y-m-d H:i:s');
      	$data = array_except($data, 'tags');

       if($post->update($data)){
            
            if(!empty(request('tags')) && \App\Tag::assignTags(request('tags'))){
                if(is_array(\Session::get('tags'))){
                    for ($i = 0; $i < count(\Session::get('tags')); $i++) {
                        if($i == 0){
                            $post->tags()->sync(\Session::get('tags')[$i]);
                            continue;
                        }
                        $post->tags()->sync(\Session::get('tags')[$i],false);
                    }
                }else{
                    $post->tags()->sync(\Session::get('tags'),true);
                }
                \Session::forget('tags');
            }
            foreach ($this->uploads as $key => $upload) {
                if(request()->hasFile($upload)){
                    $file_ext = request()->file($upload)->getClientOriginalExtension();
                    $file_name_new = sha1(str_random(40). time() ) . '.' . $file_ext;
                    $path = request()->file($upload)->move(public_path() ."/$key/",$file_name_new);
                    Post::whereTitle($post->title)->update([$upload => $file_name_new]);
                }
                continue;
            }
               
            return redirect('/posts')->with('success','Post Updated');

        }     
        return redirect()->route('posts.index')->with('error','Could not Update Post');
    }

    public function destroy(Post $post)
    {
        if(Sentinel::getUser()->id != $post->admin_id && Sentinel::getUser()->hasAccess('moderator.delete')){
            return redirect('/posts')->with('error','Unauthorized Page');
        }
        if($post->imagePath !== NULL)
        \File::delete('images/'.$post->imagePath);
        $post->delete();
        return redirect('/posts')->with('success','Post deleted');
    }

    public function downloadFile(Post $post)
    {
        if($post->exists()){
            if(\File::exists(public_path(). '/pdf/'. $post->pdfPath)){
                return response()->download(public_path() . '/pdf/'. $post->pdfPath,$post->pdfPath,['Content-Type' => 'application/pdf','Content-Description' => 'File Transfer','Content-Length' => filesize(public_path() . '/pdf/'. $post->pdfPath)]);
            }
            return back()->with('error','File Does not exist');
        }
        return back()->with('error','Invalid Record');

    }   
}
