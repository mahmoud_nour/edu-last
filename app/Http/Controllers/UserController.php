<?php

namespace App\Http\Controllers;
use Mail;

use Reminder;
use Sentinel;
use \App\User;
use Activation;
use Hash;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\checkpoints\ThrottlingException;
use Cartalyst\Sentinel\checkpoints\NotActivatedException;
class UserController extends Controller
{
   public function dashboard(){
      return view('user.dashboard');
    }
    public function postRate($username){
    	dd(request()->all());
    }
    public function getProfile($username){
    	$user = User::whereUsername($username)->first();
    	if($user){
	    	return view('user.profile')->with('user',$user);
    	}
    	return redirect()->route('user.dashboard')->with('error','Invalid Profile Name');
    }
    public function postProfile(){

		$user = Sentinel::getUser();
		$data =	request()->validate([
			'first_name' => 'string|min:3|max:16|alpha|nullable',
			'last_name'  => 'string|min:3|max:16|alpha|nullable',
			'location'   => 'string|min:3|max:32|required',
			'dob' => 'date|after:1990-01-01|before:2000-01-01|nullable',
			'username' =>[
			'min:6','max:32','string','regex:/^[a-zA-Z0-9-_.]*$/',"unique:users,username,$user->id",'nullable'
			],
			'email'       => [
			'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/','email',"unique:users,email,$user->id","nullable"
			],
			'bio' => [
			'min:3','max:254','regex:/^[a-zA-Z0-9-_.\'\" ]*$/','string','nullable'
			],
			'department' => 'nullable|string|in:civil,electrical_power,bio_medical,compuer_science,electronics_and_communication,chemistry,architecture',
			'edu_year' => 'nullable|string|in:First_Year,Second_Year,Third_Year,Forth_Year,Preparatory_Year',
			'profile_picture' => 'image|nullable|max:1999|mimes:jpg,jpeg,png,gif',
			'password'   => 'required|string|min:8|max:32'
		]);
		if(Hash::check(request('password'),$user->password)){
			if(request()->hasFile('profile_picture')){
				$file_with_ext = request()->file('profile_picture')->getClientOriginalName();
				$file_ext = request()->file('profile_picture')->getClientOriginalExtension();
				$file_name_new = str_random(40) . time() . '.' . $file_ext;
				$path = request()->file('profile_picture')->move(public_path(). '/profile_pictures/',$file_name_new);
			}
/*			dd(request()->all(),$user,$data);
*/			$user->first_name 		= request('first_name') ?? $user->first_name;
			$user->last_name  		= request('last_name') ?? $user->last_name;
			$user->email      		= request('email') ?? $user->email;
			$user->username   		= request('username') ?? $user->username;
			$user->location   		= request('location') ?? $user->location;
			$user->bio 		  		= request('bio') ?? $user->bio;
			$user->dob 		  		= request('dob') ?? $user->dob;
			$user->department 		= request('department') ?? $user->department;
			$user->edu_year 			= request('edu_year') ?? $user->edu_year;
			$user->profile_picture 	= $file_name_new ?? $user->profile_picture;

			$user->save();
			
			if($user->hasAnyAccess(['admin.*','moderator.*'])){
			
				return redirect()->route('admin.dashboard')->with('success','Profile Updated');
			}
			
			return redirect()->route('user.dashboard')->with('success','Profile Updated');
		}else{
			return redirect()->back()->with('error','Invalid Password');
		
		}
    }
}
