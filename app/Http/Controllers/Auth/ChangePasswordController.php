<?php 


namespace App\Http\Controllers\Auth;
use Hash;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\checkpoints\ThrottlingException;
use Cartalyst\Sentinel\checkpoints\NotActivatedException;

class ChangePasswordController extends Controller
{
	public function getChangePassword(){
		if(Sentinel::check()){
			return view('auth.change-password');
			
		}

		return redirect()->route('login')->with('error','You Have to login first');
	}
	public function postChangePassword(){

		if(Sentinel::check()){
			$user = \App\User::whereEmail(Sentinel::getUser()->email)->first();
			if($user){
		
				$this->validate(request(),[
					'old_password' => 'min:8|max:32|string|required',
					'password' => 'required|min:8|max:32|string|confirmed'
				]);
				if(Hash::check(request()->old_password,Sentinel::getUser()->password)){
					$user->password = Hash::make(request()->password);
					$user->save();
					//check for roles
					if(Sentinel::hasAnyAccess(['moderator.*','admin.*','superAdmin.*'])){
						//redirect to user profile
						return redirect()->route('admin.dashboard')->with('success','Password has been changed successfully');
						
					}
					return redirect()->route('user.dashboard')->with('success','Password Has been changed successfully');
				}else{
					return redirect()->back()->with('error','Invalid Password');
				}

			}else{
				return redirect()->back()->with('error','Invalid Email Address');
			}
			
		}else{
			return redirect()->route('login')->with('error','You have to login first');
		}

	}
}