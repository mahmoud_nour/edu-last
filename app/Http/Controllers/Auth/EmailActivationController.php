<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Sentinel;
use \App\User;
use Activation;
class EmailActivationController extends Controller
{
   public function activateUser($email,$token){
   
      
      if($user = User::whereEmail($email)->first()){
         $user = Sentinel::findById($user->id);
         if(Activation::exists($user)){
            if(Activation::exists($user)->code === $token){
               if(Activation::complete($user,$token)){
                  Activation::removeExpired();
                  if(Sentinel::login($user,true)){
                     return redirect()->route('user.dashboard')->with('success','Logged in Successfully');
                     
                  }
               }
            }else{
               return redirect()->route('login')->with('error','Invalid token');
            }
            
         }else{
            return redirect()->route('login')->with('error','Expired Token');
         }
      }else{
         return redirect()->route('login')->with('Invalid Email');
      }   
   }
  
}