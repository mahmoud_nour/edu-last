<?php

namespace App\Http\Controllers\Auth;
use Authy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Services\Authy\Exceptions\{InvalidTokenException,RegistrationFailedException,SmsRequestFailedException};


class AuthTokenController extends Controller
{
 	public function getToken(Request $request){
 		if(!$request->session()->has('authy')){
 			return redirect()->to('/');
 		}
 		return view('auth.token');
 	}
 	public function postToken(Request $request){
 		$this->validate($request,[
 			'token' => 'required'
 		]);
 		try {
 			$verification = Authy::verifyToken($request->token);
 			
 		} catch (InvalidTokenException $e) {

 			return redirect()->back()->withErrors([
 				'token' => 'Invalid Token'
 			]);
 		}
 		$user = \Sentinel::findById($request->session()->get('authy.user_id'));
 		if(\Sentinel::login(
 			$user,
 			$request->session()->get('authy.remember')
 		)){
 			\Session::forget('authy');
 			return redirect()->intended();
 		}
 		return redirect()->url('/');
 	}
 	public function getResend(Request $request){
 		//$request->user()
 		// you can't do that because the user is currently logged out, therefore you have to do it with session id 
 		$user = \App\User::findOrFail($request->session()->get('authy.user_id'));
 		if (!$user->hasSmsTwoFactorAuthenticationEnabled()) {
 			\Session::flash('2FA is not enabled');
 			return redirect()->back();
 		}
 		try {
        
            \Authy::requestSms($user);
        
        } catch (SmsRequestFailedException $e) {
            return redirect()->back();
        }
        $request->session()->push('authy.using_sms',true);
        return redirect()->route('token')->with('success','Token Is sent, check your phone !');
 	}
}
