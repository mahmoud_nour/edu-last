<?php 
namespace App\Http\Controllers\Auth;
use Sentinel;
use Reminder;
use Activation;
use Mail;
use \App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
  function getForgotPassword(){
    return view('auth.forgot-password');
  }
  function postForgotPassword(){
    request()->validate([
      'email' => 'required|string'
    ]);
    $user = User::whereEmailOrUsername(request('email'),request('email'))->first();
    $user = Sentinel::findById($user->id);
    if(Activation::completed($user)){
      if(count($user) === 0){
        return redirect()->route('login')->with('success','Reset Code Has been sent to your email');
      }
      $reminder = Reminder::exists($user) ?: Reminder::create($user);
      Mail::to($user)->send(new \App\Mail\ResetPassword($user,$reminder));
      return redirect()->route('login')->with('success','Reset Code Has Been sent to your email');
    }else{
      return redirect()->route('login')->with('info','You need to activate your account first');
    }
  }
  private function sendEmail($user,$token){
    Mail::send('emails.forgot-password',['user' => $user , 'token' => $token ],function($message) use($user){
      $message->to($user->email);
      $message->subject("Password Reset");
    });
  }

}