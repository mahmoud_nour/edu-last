<?php

namespace App\Http\Controllers\Auth;

use \App\Mail\Activation as userActivation;
use Mail;
use Sentinel;
use \App\User;
use Activation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
      public function getRegister(){
        return view('auth.register');
      }

   public function postRegister(Request $request){
    

     
      $this->validate($request,[
       
         'first_name' => 'required|string|min:3|max:16|alpha',
         'last_name'  => 'required|string|min:3|max:16|alpha',
         'email'       => [
            'regex:/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/','email',"unique:users,email"
         ],
         'location'   => 'required|string|min:3|max:32',
         'password'   => 'required|string|confirmed|min:8|max:32',
         'sec_question' => 'required|string|in:where_are_you_from,what_is_your_hobby,what_is_your_favorite_car,who_is_your_favorite_doctor_or_teacher',
         'sec_answer' => [
            'required',
            'min:4',
            'max:32',
            'regex:/^[a-zA-Z0-9 ]*$/',
            'string'
         ],
         'dob' => 'required|date|after:1990-01-01|before:2000-01-01',
         'department' => 'required|string|in:civil,electrical_power,bio_medical,compuer_science,electronics_and_communication,chemistry,architecture',
         'edu_year' => 'required|string|in:First_Year,Second_Year,Third_Year,Forth_Year,Preparatory_Year',
         'username' =>[
            'required','min:6','max:32','string','regex:/^[a-zA-Z0-9-_.]*$/','unique:users,username'
         ],
      ],['sec_question.in' => 'The selected option does not exist']);
      /*dd(request()->all());*/
      $user = Sentinel::register([
         'first_name' => request()->first_name,
         'last_name'  => request()->last_name,
         'location'   => request()->location,
         'password'   => request()->password,
         'email'      => request()->email,
         'sec_question' => request()->sec_question,
         'sec_answer' => request()->sec_answer,
         'dob' => request()->dob,
         'department' => request()->department,
         'username' => request('username'),
         'edu_year'  => request('edu_year'),
      ]);
      $activation = Activation::create($user);

      $role = Sentinel::findRoleBySlug('user');
      

      $role->users()->attach($user);
      Mail::to($user)->send(new userActivation($user,$activation));
    // $this->sendEmail($user,$activation->code);
      return redirect()->route('login')->with('success','Thanks for Registering, please go and activate your account');

   }
   

}
