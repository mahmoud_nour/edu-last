<?php

namespace App\Http\Controllers\Auth;
use Sentinel;
use \App\User;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Checkpoints\{NotActivatedException,ThrottlingException};
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
  private $redirectToToken = '/user/token';
  public function getLogin(){
    return view('auth.login');
  }
  public function postLogin(){
      //email validation laravel 5.5 custom validation
    $this->validate(request(),[
      'email' => 'required',
      'password' => 'required|string|min:8|max:32',
      'remember' => 'in:on,null'
       
    ],['remember.in' => 'Invalid Value']);
    $remember = false;
    if(request('remember') === 'on'){
      $remember = true;
    }
    
  
  
    try {
      $user = Sentinel::authenticate(['login' => request('email'), 'password' => request('password')],$remember);
      if($user){
        if($user->hasTwoFactorAuthenticationEnabled()){

          return $this->logoutAndRedirectToTokenEntry(request(),$user);
        }else{
          if($user->hasAnyAccess(['admin.*','moderator.*'])){
            return redirect()->intended('/')->with('success','Welcome to the admin dashboard');
          }elseif($user->hasAccess('user.*')){
            return redirect()->intended(route('posts.index'))->with('success','Logged in Successfully');
          }
          
        }
                
      }
         
      return redirect()->back()->with('error','Invalid Credentials');
            
    } catch (NotActivatedException $e) {
      return redirect()->back()->with('error','Perhaps you forgot to activate your account');
    }catch(ThrottlingException $e){
      return redirect()->back()->with('error', $e->getMessage());
    }
     
  }

  public function logout(){
  
    Sentinel::logout(null,true);
  
    return redirect()->route('login')->with('success','Come back again whenever you can ');
  }
  protected function logoutAndRedirectToTokenEntry(Request $request,User $user){
        Sentinel::logout(null,true);
        $request->session()->put('authy',[
            'user_id' => $user->id,
            'authy_id' => $user->authy_id,
            'using_sms' => false,
            'remember' => $request->has('remember')
        ]);
        if($user->hasSmsTwoFactorAuthenticationEnabled()){
            try {

                \Authy::requestSms($user);
            } catch (SmsRequestFailedException $e) {
                return redirect()->back();
            }
            $request->session()->push('authy.using_sms',true);
        }
        return redirect($this->redirectTokenPath());
    }
    protected function redirectTokenPath(){
      return $this->redirectToToken;
    }
}
