<?php

namespace App\Http\Controllers;
use \App\Like;
use Illuminate\Http\Request;
use Sentinel;
class LikeController extends Controller
{
   public function like($type,$id){

        $like = new Like;
        if(Like::handle($type,$id)){
          if($like->status($type,$id) == false){
            $like = $like->where(['likable_type' => $type,'likable_id' => $id]);
            if($like->exists()){
                  if($like->update(['like_status' => 1 , 'created_at' => date('Y-m-d H:i:s')])){
                    return back()->with('success','Liked !');
                  }
              
            }
          }else{
              return back()->with('info','Already Liked !');
          }

          }
        $like->user_id = Sentinel::getUser()->id;
        $like->like_status = 1;
        $like->likable_type = $type;
        $like->likable_id = $id;
        $like->created_at = date('Y-m-d H:i:s');
        $like->save();
        return back()->with('success','Liked !');
  }
  public function reset($type,$id){

    if(is_bool(Like::status($type,$id))){
      if($like = Like::handle(class_basename($type),$id)){
        if($like->delete()){
          return back()->with('success','reset');
        }
      }
    }
    return back()->with('error',' could not reset');
  }

  public function dislike($type,$id){

     $like = new Like;
        if(Like::handle($type,$id)){

          if($like->status($type,$id)){
            $like = $like->where(['likable_type' => $type,'likable_id' => $id]);
            if($like->exists()){
                  if($like->update(['like_status' => 0])){
                    return back()->with('success','Disliked !');
                  }
              
            }
          }else{
              return back()->with('info','Already Disliked !');
          }

        }
        $like->user_id = Sentinel::getUser()->id;
        $like->like_status = 0;
        $like->likable_type = $type;
        $like->created_at  = date('Y-m-d H:i:s');
        $like->likable_id = $id;
        $like->save();
        return back()->with('success','Disliked !');

  }
}