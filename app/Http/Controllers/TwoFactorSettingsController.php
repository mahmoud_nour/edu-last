<?php

namespace App\Http\Controllers;
use Authy;
use \App\DiallingCode;
use Illuminate\Http\Request;
use \App\Services\Authy\Exceptions\{InvalidTokenException,RegistrationFailedException,SmsRequestFailedException};


class TwoFactorSettingsController extends Controller
{
    public function index(){
    	return view('settings.twofactor')->with([
    		'diallingCodes' => DiallingCode::all()
    	]);
    }
    public function update(){
    	$this->validate(request(),[
    		'two_factor_type' => 'required|in:'. implode(',', array_keys(config('twofactor.types'))),
    		'phone_number' => 'required_unless:two_factor_type,off|max:12',
    		'dialling_code' => 'required_unless:two_factor_type,off|exists:dialling_codes,id',
        ]);
        $user = request()->user();
    	$user->updatePhoneNumber(request('phone_number'),request('dialling_code'));
    	if(!$user->registeredForTwoFactorAuthentication()){
    		try {
    			$authyId = Authy::registerUser($user);
    			$user->authy_id = $authyId;
    		} catch (RegistrationFailedException $e) {
    			return redirect()->back()->with('error','Error Occured');
    		}
    	}

    	$user->two_factor_type = request('two_factor_type');
    	$user->save();
    	return redirect()->back()->with('success','2FA Is Enabled !');
    }

}
