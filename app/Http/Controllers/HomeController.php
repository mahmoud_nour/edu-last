<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Post;
use Sentinel;
class HomeController extends Controller
{
     public function index(){
     	$post = new Post;
     	return view('home')->with('RecentPosts' , $post->RecentPostsDependingOnBranch(Sentinel::getUser()->edu_year ?? NULL, Sentinel::getUser()->department ?? NULL));
     }
}
