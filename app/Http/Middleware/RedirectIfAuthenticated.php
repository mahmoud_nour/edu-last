<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
       
        if (Sentinel::check() && Sentinel::getUser()->hasAnyAccess(['admin.*','moderator.*'])) {
            return redirect()->route('admin.dashboard')->with('info','you are already logged in');
        }elseif(Sentinel::check() && Sentinel::getUser()->hasAccess('user.*')){
            return redirect()->route('user.dashboard')->with('info','you are already logged in');
        }

        return $next($request);
    }
}
