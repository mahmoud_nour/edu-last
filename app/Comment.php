<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use Notifiable;
    public function post(){
   		return $this->belongsTo(Post::class);
    }
    public function user(){
   		return $this->belongsTo(User::class);
    }
    public function editor(){
    	return $this->belongsTo(User::class,'edited_by','id');
    }

    public function replies(){
    	return $this->hasMany(Reply::class,'comment_id');
    }

     public function likes(){
       return$this->morphMany(Like::class,'likable');
    }

  
}
