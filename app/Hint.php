<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hint extends Model
{
	protected $table = 'hints';
   
    public static function getTerm()
    {
        //term
        $term = \Carbon\Carbon::now();
        $i = 0;
        foreach ( static::where(['year' => request('edu_year')])->pluck('term_duration')->toArray() as $value) {
            if (in_array($term->month,explode(',',$value))){
                if ($i == 0) {
                    return 'first_term';
                }
                return 'second_term';
            }
            $i++;
        }
    }
    
    public static function menuElements()
    {
    	foreach (static::pluck('year')->unique() as $year) {
            foreach (static::where('year',$year)->get() as $record) {
    			if ($year == 'Preparatory_Year' && $record->branches != 'general') {
    			
    				// exploding null returns an element with an empty string
    				$branches = explode(',', $record->branches);
    				foreach ($branches as $branch) {
    					$menus[$branch][$record->year][$record->term][] = static::where(array('year' => $year,'term' => $record->term))->get(array('subjects'));
    				}
    				
    				
    				
    			}else {
                    
    				$menus[$record->branches][$record->year][$record->term][] = static::where(array('year' => $year,'term' => $record->term))->get(array('subjects'));

    			}
    		}
    		
    	
    	}
    	return $menus ?? NULL;
    }
}
