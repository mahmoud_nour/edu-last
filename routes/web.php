<?php 
Route::get('/subject/{subject}/{category?}','PostController@listBySubject')->name('posts.category.index');
Route::redirect('/','/home');
Route::get('/home','HomeController@index')->name('home');
Route::get('archives/{year}/{month}','PostController@getByArchive')->name('archives');
Route::group(['namespace' => 'Auth'],function(){
	Route::group(['middleware' => 'guest'],function(){
		Route::get('/user/token','AuthTokenController@getToken')->name('token');
		Route::get('/token/resend','AuthTokenController@getResend')->name('resend-token');
		Route::post('/user/token','AuthTokenController@postToken')->name('token');
		Route::get('/register',[
			'uses' => 'RegisterController@getRegister',
			'as' => 'register'
		]);
		Route::post('/register',[
			'uses' => 'RegisterController@postRegister',
			'as' => 'register'
		]);
		Route::get('/login',[
			'uses' => 'LoginController@getLogin',
			'as' => 'login'
		]);
		Route::post('/login',[
			'uses' => 'LoginController@postLogin',
			'as' => 'login'
		]);
	
		

		Route::get('/activate/{email}/{token}','EmailActivationController@activateUser');
		Route::get('/resetpassword','ForgotPasswordController@getForgotPassword')->name('reset');
		Route::post('/resetpassword','ForgotPasswordController@postForgotPassword')->name('reset');
		Route::get('/reset/{email}/{token}','ResetPasswordController@getPasswordResetThroughEmail')->name('reset-password');
		Route::post('/reset-password','ResetPasswordController@postPasswordResetThroughEmail')->name('reset-password');
		Route::get('/resetBySecurityQuestion' , ['uses' => 'ResetPasswordController@getPasswordResetThroughQuestion','as' => 'reset.security']);
		Route::post('/resetBySecurityQuestion/stage1' , ['uses' => 'ResetPasswordController@postPasswordResetThroughQuestion1','as' => 'reset.security1']);
		Route::post('/resetBySecurityQuestion/stage2' , ['uses' => 'ResetPasswordController@postPasswordResetThroughQuestion2','as' => 'reset.security2']);
		Route::post('/resetBySecurityQuestion/stage3' , ['uses' => 'ResetPasswordController@postPasswordResetThroughQuestion3','as' => 'reset.security3']);
	});
});

// middleware
Route::group(['middleware' => 'user'],function(){
	Route::get('/dashboard','UserController@dashboard')->name('user.dashboard');
	Route::get('/profile/{username}',['uses' => 'UserController@getProfile','as' =>'profile']);
	Route::get('/profile/{username}/viewas',['uses' => 'UserController@getProfile','as' =>'profile.viewas']);
	Route::post('/profile',['uses' => 'UserController@postProfile','as' => 'profile']);
	Route::post('/rate/{username}',['uses' => 'UserController@postRate','as' => 'rating']);
		Route::post('/change-password',[
		'uses' => 'Auth\ChangePasswordController@postChangePassword',
		'as' => 'change-password'
	]);
	Route::get('/change-password',[
		'uses' => 'Auth\ChangePasswordController@getChangePassword',
		'as' => 'change-password'
	]);
	Route::post('logout',[
		'uses' => 'Auth\LoginController@logout',
		'as' => 'logout',
	]);	
	Route::post('/comments/{post}','CommentController@store')->name('comments.store');
	Route::put('/comments/{comment}','CommentController@update')->name('comments.update');
	Route::delete('/comments/{comment}','CommentController@destroy')->name('comments.destroy');
	Route::get('/replies/{reply}/{post}','RepliesController@edit')->name('replies.edit');
	Route::post('/replies/{comment}/{post}','RepliesController@store')->name('replies.store');
	Route::put('/replies/{reply}/{comment}/{post}','RepliesController@update')->name('replies.update');
	Route::delete('/replies/{reply}','RepliesController@destroy')->name('replies.destroy');
	Route::get('/comments/{comment}/{post}','CommentController@edit')->name('comments.edit');
	Route::post('/download/{post}','PostController@downloadFile')->name('posts.download');
	Route::get('/settings/twofactor','TwoFactorSettingsController@index')->name('2fa-settings');
	Route::put('/settings/twofactor','TwoFactorSettingsController@update');

	Route::post('/like/{type}/{id}',[
		'uses' => 'LikeController@like',
		'as'	=> 'like'
	]);
	Route::post('/dislike/{type}/{id}',[
		'uses' => 'LikeController@dislike',
		'as'	=> 'dislike'
	]);
	Route::post('/reset/like/{type}/{id}',[
		'uses' => 'LikeController@reset',
		'as'	=> 'reset-like'
	]);




	Route::get('/markAsRead',function(){
		\Sentinel::getUser()->unreadNotifications->markAsRead();
	});
});
	Route::group(['middleware' => 'admin'],function(){
		Route::view('/admin','admin.dashboard');
		Route::resource('/posts','PostController');
		Route::get('/upgrade','AdminController@listUsers')->name('list.users');
		Route::get('/downgrade','AdminController@listUsers');
		Route::post('/downgrade/{username}','AdminController@downgradeUser')->name('downgrade.users');
		Route::post('/upgrade/{username}','AdminController@upgradeUser')->name('upgrade.users');
		Route::get('/unapproved',['uses' => 'PostController@unApproved','as' => 'posts.unapproved']);
		Route::post('/posts/approve/{id}',['uses' => 'AdminController@approvePost','as' =>'posts.approve']);
		Route::resource('/tags','TagController');
		Route::get('/popular/tags','TagController@sortByPopularity');
		Route::put('/approve/comments/{comment}','CommentController@approveComment')->name('comments.approve');
		Route::put('/approve/replies/{reply}','RepliesController@approveReply')->name('replies.approve');
	});
	
	Route::get('/posts','PostController@index')->name('posts.index');
	Route::get('/posts/{post}','PostController@show')->name('posts.show');
	Route::get('/tags/{tag}','TagController@show')->name('tags.show');
Route::get('/comments/{comment}','CommentController@show')->name('comments.show');
Route::get('/replies/{reply}','RepliesController@show')->name('replies.show');
Route::get('/test',function(){
	dd(\App\Hint::menuElements());
	dd(array_merge(request()->all(),['reply' => Route::current()->parameter('reply')]));
	dd(request()->route('reply'));
	dd(Route::input('reply'));
	dd(request()->route()->parameter('reply'));
	$user = Sentinel::findById(10);
	$post = \App\Post::find(5);
	foreach ($post->RecentPostsDependingOnBranch($user->year,$user->department) as $posts) {
		dd($posts);
		foreach ($posts as $post) {
			echo $post->title . '<hr/>';
		}
	}
	//dd(\App\Post::find(5)->hints->first()->);	
});
?>
