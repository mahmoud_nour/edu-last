@extends('layouts.master')
@include('layouts.navbar')
@section("header")
      <header class="col-md-12">
      </header><!-- header-->
@endsection
@section('container-row')
        <div class="col-md-9">
             <div class="page page-content">
                <article class="post-content">
                  <figure class="img-overflow"> <!-- article-->
                    <img src="http://via.placeholder.com/440x284png" alt="placeholder">
                  </figure>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              <a href="#">Block by Block</a>.
                              </p>

                  <figure class="img-overflow"> <!-- article-->
                    <img src="http://via.placeholder.com/440x284" alt="placeholder">
                  </figure>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              <a href="#">Block by Block</a>.
                              </p>
                </article>
            </div>
        </div> <!--  col-md-9-->
@endsection

@section('side-bar')
        <aside class="col-md-3 side-bar">
        <!--
          <div class="row">
	<div class="col-sm-6 col-md-12">
	  <h2 class="side-bar__title">Recent posts</h2>
	  <ul class="recent-posts list-unstyled">
	      <li class="recent-posts__item list-item">
	          <a class="recent-posts__link post-link" href="#">
	          Lorem Ipsum is simply dummy text of the printing and typesetting </a>
	      </li>
	  </ul>
	</div>
	<div class="col-sm-6 col-md-12">
	     <h2 class="side-bar__title">Categories</h2>
	    <ul class="list-categories list-unstyled">
	        <li class="list-categories__item list-item">
	            <a class="list-categories__link" href="#">
	               Lorem Ipsum is simply dummy text of the printing and typesetting 
	            </a>
	        </li>
	  </ul>
	</div>
          </div> 
          -->

              <h2 class="side-bar__title">Follow us</h2>
            <ul class="social-links list-inline">
                <li><a href="https://www.facebook.com/">
                <img src="{{ asset('user/svg/icon-facebook.svg') }}" width="32=&quot;true&quot;" height="32=&quot;true&quot;" alt="icons/icon-facebook.svg"></a></li>
                 <li><a href="https://twitter.com/">
                <img src="{{ asset('user/svg/icon-twitter.svg') }}" width="32=&quot;true&quot;" height="32=&quot;true&quot;" alt="icons/icon-twitter.svg"></a></li>
                <li><a href="https://www.youtube.com/user/">
                <img src="{{ asset('user/svg/icon-youtube.svg') }}" width="32=&quot;true&quot;" height="32=&quot;true&quot;" alt="icons/icon-youtube.svg"></a></li>
            </ul>
        </aside> <!-- aside-->
@endsection