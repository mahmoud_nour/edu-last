@extends('layouts.app')
<style>
/*
rating
*/
@import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

fieldset, label { margin: 0; padding: 0; }
body{ margin: 20px; }
h1 { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
/*
profile
 */
#app > div > div > div.col-md-9 > div > div.panel-body > div > div.row > div > div > img{
    min-height: 0px;
    padding-left: 0px;
    padding-right: 0px;	
}
.img-thumbnail{
	padding: 0px !important;
}
.uname{
	margin-top: 40px;
	margin-left: 10px !important;
}	
</style>
@section('panel-heading')		
<p class="text-center"><img src="{{ asset('images/033648a8a98b4398c27395f81ca6c07ab7ff8cf1.png') }}" style="max-width: 50px;max-height: 50px;border-radius: 50%;">
 {{ $user->first_name .' ' .$user->last_name.'\'s ' }} Profile</p>
@endsection

@section('content')

	@if (strpos(request()->path(), 'viewas') || Sentinel::getUser()->id !== $user->id)
	<div class="container" style="padding-top: 10px;">
  <h1 class="page-header"> Profile</h1>
  <div class="row">
    <!-- left column --> 
    <!-- edit form column -->
    <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
          <div class="list-group"> 
         <img src="/profile_pictures/default.png" class=" col-sm-6 avatar img-circle img-thumbnail pull-left" alt="avatar" style="max-width: 100px; max-height: 100px; border-radius: 50%;">
         <h3 class="col-sm-6 uname">{{ $user->first_name }}</h3>
       

		<form action="{{ route('rating',$user->username) }}" method="POST" class="rating" id="rating-form">
				{{ csrf_field()}}

			 <input type="radio" id="star" name="rating" value="5" /><label class = "full" for="star5" title="Awesome"></label>
	    <input type="radio" id="star" name="rating" value="4.5" /><label class="half" for="star4half" title="Pretty Good"></label>
	    <input type="radio" id="star" name="rating" value="4" /><label class = "full" for="star4" title="Good"></label>
	    <input type="radio" id="star" name="rating" value="3.5" /><label class="half" for="star3half" title="Kinda Good"></label>
	    <input type="radio" id="star" name="rating" value="3" /><label class = "full" for="star3" title="Satisfied"></label>
	    <input type="radio" id="star" name="rating" value="2.5" /><label class="half" for="star2half" title="Nearly Satisfied"></label>
	    <input type="radio" id="star" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad"></label>
	    <input type="radio" id="star" name="rating" value="1.5" /><label class="half" for="star1half" title="Just sucks"></label>
	    <input type="radio" id="star" name="rating" value="1" /><label class = "full" for="star1" title="Really sucks"></label>
	    <input type="radio" id="star" name="rating" value=".5" /><label class="half" for="starhalf" title="Burn in hell"></label>
			
		</form>
           <div class="clearfix"></div>
           <!-- <small> {{-- $user->rates->avg('rate') }} is your rate</small>
	 <small>{{ $user->rates->first()->user->username --}} Rated You</small>
	 -->
        </div>
      <h3>Personal info</h3> 
        <ul class="list-group">
	  	<li class="list-group-item">Name : {{ $user->username }}</li>
	  	<li class="list-group-item">bio : {{ $user->bio }}</li>
	  	<li class="list-group-item">email : {{ $user->email }}</li>
	  	<li class="list-group-item">department : {{ str_replace('_',' ',$user->department )}}</li>
	  	<li class="list-group-item">Age : {{ $user->age() }} Years</li>
        </ul>
       </div>
  </div>
    <div class="col-md-8 col-sm-6 col-xs-12 personal-info">

<h3>User Activity</h3> 
	  <ul class="list-group">
	  	@if($user->comments)
	  	<li class="list-group-item">Comments : {{ $user->comments->count() }}</li>
	  	@else
	  	<li class="list-group-item">Comments : 0</li>
	  	@endif
	  	@if($user->likes)
	  	<li class="list-group-item">Likes : {{ $user->likes->count() }}</li>
	  	@else
	  	<li class="list-group-item">Likes : 0</li>
	  	@endif
	  	@if(\Sentinel::findById($user->id)->hasAnyAccess(['*.create','*.edit']))
	  		@if($user->tags )
		  	<li class="list-group-item">Tags : {{ $user->tags->count() }}</li>
		  	@else
		  	<li class="list-group-item">Tags : 0</li>
		  	@endif
		  	@if($user->posts )
		  	<li class="list-group-item">Posts : {{ $user->posts->count() }}</li>
		  	@else
		  	<li class="list-group-item">Posts  : 0</li>
		  	@endif
	  	@endif
	


	  </ul>
</div>	  
</div>
	
	  
	@elseif(Sentinel::getUser()->id === $user->id)
		<p><a href="{{ route('profile.viewas',$user->username)}}" class="btn btn-default pull-left">View As</a></p>
		<form action="{{ route('profile')}}" method="POST">
			{{csrf_field()}}
			<div class="form-group">
					<label for="Email"> Email </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="email" class="form-control"  placeholder="email " name="email" value="{{ old('email') }}">
				</div>
			</div>
			<div class="form-group">
							<label for="username"> Username </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" class="form-control" placeholder="Ex : User_name" name="username" value="{{ old('username')}}" >
				</div>

			<div class="form-group">
							<label for="first_name"> First Name </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" class="form-control" placeholder="John" name="first_name" value="{{ old('first_name')}}" >
				</div>
			</div>
			<div class="form-group">
			<label for="last_name"> Last Name </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" class="form-control" placeholder="Doe" name="last_name" value="{{ old('last_name')}}" >
				</div>
			</div>

			<div class="form-group">
			<label for="Location"> Location </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
					<input type="text" class="form-control" placeholder="United States" name="location" value="{{ old('location')}}" >
				</div>
			</div>
			
			<div class="form-group">
			<label for="dob"> Date Of Birth </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
					<input type="date" class="form-control" name="dob" value="{{ $user->dob }}" >

				</div>
			</div>
			<div class="form-group">
			<label for="Bio"> Bio </label>
			
					<textarea class="form-control" name="bio"> {{ old('bio') }} </textarea>
			
			</div>
		
			<div class="form-group">
			<label for="department"> Department</label>
				<select class="form-control" name="department">
				  <option selected disabled>Pick Up A Department</option>
				  	<option value="electronics_and_communication">Electronics & Communication</option>
				  <option value="civil">Civil</option>
				  <option value="electrical_power">Electrial Power</option>
				  <option value="bio_medical">Bio Medical</option>
				  <option value="chemistry"> Chemistry </option>
				  <option value="architecture">Architecture</option>
				</select>
			</div>
			<div class="form-group">
			<label for="year">Education Year</label>
				<select class="form-control" name="year">
				  <option selected disabled>Pick Up A Year</option>
				  	<option value="First_Year">First Year</option>
				  <option value="Second_Year">Second Year</option>
				  <option value="Third_Year">Third Year</option>
				  <option value="Forth_Year">Forth Year</option>
				</select>
			</div>
			<div class="form-group">
				<label for="profile"> Profile Picture</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-upload"></i></span>
					<input type="file" name="profile" class="form-control-static"> 
				</div>
			</div>

			<div class="form-group">
			<label for="Password"> Type Your Password To Verify It's you </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input type="password" class="form-control" placeholder="Password" name="password" required>
				</div>
			</div>
	<div class="form-group">
		
			<button type="submit" class="btn btn-success form-control">
					<i class="fa fa-handshake-o" aria-hidden="true"></i>
				Update Profile
			</button>
		
	</div>
</form>
</div>

	@endif


@endsection
