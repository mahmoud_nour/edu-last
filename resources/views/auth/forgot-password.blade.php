@extends('layouts.app')

@section('panel-heading')		
		<p class="text-center">Reset Your Password</p>
@endsection

@section('content')
	<form action="{{ route('reset')}}" method="POST">
		{{csrf_field()}}
		<div class="form-group">
				<label for="Email"> Email </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				<input type="email" class="form-control" placeholder="example@example.com" name="email">
			</div>
		</div>
		<div class="form-group">
			
				<button type="submit" class="btn btn-success form-control">
   					<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
					Send Code
				</button>
			<small><p class="text-center" ><a href="{{ route('reset.security') }}">Reset Password By Security Questions?</a>	</p></small>
		</div>
	</form>

@endsection