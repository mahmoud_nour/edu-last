@extends('layouts.app')

@section('panel-heading')		
		<p class="text-center">Login Form</p>
@endsection

@section('content')
	<form action="{{ route('login')}}" method="POST" id="login-form">
		{{csrf_field()}}
		<div class="form-group">
				<label for="Email"> Email | Username</label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				<input type="text" class="form-control" placeholder="example@example.com" name="email">
			</div>
		</div>

	
		<div class="form-group">
		<label for="Email"> Password </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock"></i></span>
				<input type="password" class="form-control" placeholder="Password" name="password">
			</div>
		</div>
		
		<div class="form-group">
			
				<button type="submit" class="btn btn-success form-control">
   					<i class="fa fa-handshake-o" aria-hidden="true"></i>
					Login
				</button>
			<small><p class="text-center" ><a href="{{ route('reset') }}">Forgot Your Password?</a>	</p></small>
		</div>
	</form>

@endsection
@section('scripts')
	<script type="text/javascript">
		$.ajaxSetup({
			headers : {
				'X-CSRF-TOKEN' : $('meta[name="csrf_token"]').attr('content')
			}
		})
		$('#login-form').submit(function(event){
			event.preventDefault()
			var postData = {
				'email' : $('input[name=email]').val,
				'password' : $('input[name=password]').val,
				//'remember_me' : $('input[name=remember_me]').is(':checked'),
			}
			$.ajax({
				type:'POST', 
				url : '/login',
				data: postData
			})
		});
	</script>
@endsection