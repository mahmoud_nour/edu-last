@extends('layouts.app')

@section('panel-heading')		
		<p class="text-center">Reset Your Password</p>
@endsection

@section('content')
	<form action="{{ route('reset-password')}}" method="POST">
		{{csrf_field()}}
		<div class="form-group">
				<label for="Password"> Password </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock"></i></span>
				<input type="password" class="form-control" placeholder="Password" name="password">
			</div>
		</div>
		<div class="form-group">
				<label for="Password"> Password Confirmation </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock"></i></span>
				<input type="password" class="form-control" placeholder="Password" name="password_confirmation">
			</div>
		</div>
		<div class="form-group">
			
				<button type="submit" class="btn btn-success form-control">
   					<i class="fa fa-spinner fa-pulse fa fa-fw"></i>
				<span class="sr-only"></span>
					Change Password
				</button>
			<small><p class="text-center" ><a href="{{ route('reset.security') }}">Reset Password By Security Questions?</a>	</p></small>
		</div>
	</form>

@endsection