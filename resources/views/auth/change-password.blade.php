@extends('layouts.master')

@section('panel-heading')		
		<p class="text-center">Change Password</p>
@endsection

@section('content')
 
	<form action="{{ route('change-password') }}" method="POST">
		{{csrf_field()}}

		<div class="form-group">
				<label for="current_password"> Your Current Password </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock"></i></span>
				<input type="password" class="form-control" placeholder="Example : 123456" name="old_password">
			</div>
		</div>
		<div class="form-group">
				<label for="password"> Your New Password </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock"></i></span>
				<input type="password" class="form-control" placeholder="Example : 123456" name="password">
			</div>
		</div>
		<div class="form-group">
				<label for="password_confirmation"> Confirm Your Password </label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-lock"></i></span>
				<input type="password" class="form-control" placeholder="Example : 123456" name="password_confirmation">
			</div>
		</div>
		<div class="form-group">
			
				<button type="submit" class="btn btn-success form-control">
   					<i class="fa fa-paper-plane-o" aria-hidden="true"></i>
					Send Code
				</button>
			<small><p class="text-center" ><a href="{{ route('reset.security') }}">Reset Password By Security Questions?</a>	</p></small>
		</div>
	</form>

@endsection