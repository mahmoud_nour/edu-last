@extends('layouts.app')
@section('style')
 
<link href="{{ asset('css/comment.css') }}" rel="stylesheet">

@endsection
@section('panel-heading')
	<p class="text-center">{{ $tag->posts->count() . ' Posts have ' . strtoupper($tag->name) . ' Tag'}}</p>
@endsection

@section('content')
	@if (count($tag->posts) > 0)
		@foreach ($tag->posts()->where('approved',1)->get() as $post)
		<div class="row">
			
				 <div class="panel panel-white post panel-shadow">
		            <div class="post-heading">
		               <div class="pull-left image"> 
		                  <img src=" {{ asset($post->admin->profile_picture  ?? 'default.png') }}" class="img-circle avatar" alt="user profile image" style="max-width: 50px;max-height: 50px;border-radius: 50%">
		               </div>
		               <div class="pull-left meta">
		                  <div class="title h5">
		                     <a href="/profile/{{ $post->admin->username }}"><b>{{ $post->admin->first_name . ' ' . $post->admin->last_name }}</b></a>
		                     created a post
		                     <p> <small>{{ $post->admin->roles->first()->slug }}</small></p>
		                  </div>
		                  <h6 class="text-muted time"> {{ $post->created_at }}</h6>
		               </div>
		            </div>
		      </div>

		      <div class="post-description"> {{-- post-description --}}
		      	<p>   {{ $post->body }}</p> 
		        </div>
		</div>

<hr>
		@endforeach
			
	@endif
@endsection