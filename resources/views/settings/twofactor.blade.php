@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="{{-- panel panel-default --}}">
                <div class="panel-heading">Two Factor Settings</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('/settings/twofactor') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('two_factor_type') ? ' has-error' : '' }}">


                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <select name="two_factor_type" class="form-control">
                                        @foreach (config('twofactor.types') as $key => $name)
                                            <option value="{{ $key }}"  {{ ( old('two_factor_type') === $key) || (Sentinel::getUser()->hasTwoFactorType($key)) ? "selected" :  '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                @if ($errors->has('two_factor_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('two_factor_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                            <div class="form-group{{ $errors->has('dialling_code') ? ' has-error' : '' }}">
                            <label for="dialling_code" class="col-md-4 control-label">Dialling Code</label>

                            <div class="col-md-6">
                                <select class="form-control" name="dialling_code">
                                   @foreach ($diallingCodes as $diallingCode)
                                        <option value="{{ $diallingCode->id}}" {{ old('dialling_code') === $diallingCode->id || Sentinel::getUser()->hasDiallingCode($diallingCode->id) ? 'selected="selected"' : '' }}> {{ $diallingCode->name }} (+{{$diallingCode->dialling_code}}) </option>
                                   @endforeach
                                </select>

                                @if ($errors->has('dialling_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dialling_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                            <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone_number" type="text" class="form-control" value="{{ old('phone_number')  ? old('phone_number') : Sentinel::getUser()->getPhoneNumber() }}" name="phone_number" required>

                                @if ($errors->has('phone_number'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                {{ method_field('PUT') }}
                                <button type="submit" class="btn btn-primary">
                                    Update Settings
                                </button>


                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
