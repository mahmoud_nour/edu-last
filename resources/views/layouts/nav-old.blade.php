<style type="text/css">

.dropdown-submenu {
    position: relative;
}

.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}
.navbar {
     margin-top: 0 !important;
    margin-bottom: 0 !important;
}

</style>

 <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-left">
                     <li><a href="/">home</a></li>
                     @if( Sentinel::getUser('admin','moderator','user'))
                    @endif
                    <li><a href="{{ url('/posts') }}">posts</a></li> <!--  drop menu-->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                               list <span class="caret"> </span></a>
                                    <ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                                        @foreach ($menus as $menu)
                                      <li class="dropdown-submenu">
                                        <a tabindex="-1" href="#"> {{  $menu->branches }}</a>
                                        <ul class="dropdown-menu">
                                              <li class="dropdown-submenu">
                                                <a href="#">{{  $menu->year }}</a>
                                                <ul class="dropdown-menu">
                                                    <li class="dropdown-submenu">
                                                        <a href="#">{{  $menu->term }}</a>
                                                        <ul class="dropdown-menu">
                                                                @foreach (explode(',',$menu->subjects ) as $subject)
                                                                        <li><a href="#"> {{ $subject }}</a></li>
                                                                @endforeach
                                                        </ul>
                                                    </li>
                                                </ul>
                                              </li>
                                        </ul>
                                        @endforeach
                                      </li>
                                    </ul>
                            </li>  {{-- end -drop menu --}}

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if(Sentinel::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    @if (Sentinel::getUser()->profile_picture)
                                        @php
                                            $profile_picture = Sentinel::getUser()->profile_picture;
                                        @endphp
                                        <img src="{{ asset("$profile_picture") }}" style="max-width: 30px;max-height: 50px;border-radius: 50%">
                                    @else
                                    <span><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i></span>
                                    @endif
                                    {{ Sentinel::getUser()->first_name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li> <a href="{{ route('2fa-settings') }}">
                                           Two Factor Authentication Settings
                                        </a></li>
                                </ul>
                            </li>
                            {{-- Notification Here --}}
                            <notification :userid="{{Sentinel::getUser()->id }}" :unreads="{{ Sentinel::getUser()->unreadNotifications }}"></notification>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>