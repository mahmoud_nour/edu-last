<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <title>Edu</title>
  <meta name="description" content="">
  <link href="{{ asset('user/css/main.css') }}" rel="stylesheet">
  <link href="{{ asset('user/css/style.css') }}" rel="stylesheet">
  <link rel="canonical" href="#">
  <link rel="alternate" type="application/atom+xml" title="Edu" href="/feed.xml">
  <link rel="icon" type="image/png" href="http://icons.iconarchive.com/icons/hydrattz/multipurpose-alphabet/16/Letter-E-blue-icon.png" sizes="16x16">
    <style type="text/css">/* This is not a zero-length file! */</style>
    <style type="text/css">/* This is not a zero-length file! */</style>
    <style>
@import url('https://fonts.googleapis.com/css?family=Raleway');
</style>

    <link rel="stylesheet" href="{{ url('dmenu/css/reset.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ url('dmenu/css/style.css') }}"> <!-- Resource style -->
    <script src="{{ url('dmenu/js/modernizr.js') }}"></script> <!-- Modernizr -->

</head>

<body data-gr-c-s-loaded="true">
<div class="container-fluid">
  <div class="row site-header">
    <div class="container ">
      <div class="row">
        @yield("header")
        <!-- header-->
      </div>
    </div>
  </div>
</div>

<div class="row site-body">
  <div class="container">
        @include('layouts.messages')
    <div class="row">
      @yield('container-row')
        <!--  col-md-9-->
        @include('layouts.sidebar')
        @yield("side-bar")
         <!-- aside-->
    </div>
  </div>
</div>

<footer class="row site-footer">
  <div class="container">
    <div class="row site-footer__body">
      <div class="col-md-12 text-center">
        <p>© 2017 </p>
        <p><a href="#">Terms of Use</a></p>
        <p><a href="#">Privacy Policy</a></p>
      </div>
    </div>
  </div>
</footer>

      <script src="{{ asset('user/js/site.js') }}"></script>
      <script src="{{ asset('user/js/site2.js') }}"></script>
        <script type="text/javascript">
    $.ajaxSetup({
      headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf_token"]').attr('content')
      }
    })
    $('#login-form').submit(function(event){
      event.preventDefault()
      var postData = {
        'email' : $('input[name=email]').val,
        'password' : $('input[name=password]').val,
        //'remember_me' : $('input[name=remember_me]').is(':checked'),
      }
      $.ajax({
        type:'POST',
        url : '/login',
        data: postData
      })
    });
  </script>
      <!-- Scripts -->
        <script
          src="https://code.jquery.com/jquery-3.2.1.min.js"
          integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
          crossorigin="anonymous">
      </script>
     <script src="{{ asset('js/notification.js') }}"></script>

<script src="{{ url('dmenujs/jquery-2.1.1.js') }}"></script>
<script src="{{ url('dmenu/js/jquery.menu-aim.js') }}"></script> <!-- menu aim -->
<script src="{{ url('dmenu/js/main.js') }}"></script> <!-- Resource jQuery -->

</body>

  </span>
</html>