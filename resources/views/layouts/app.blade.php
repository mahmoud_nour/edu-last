<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <title>Edu</title>
    @yield('style')
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      {{-- <link href="{{ asset('user/css/main.css') }}" rel="stylesheet"> --}}

    {{-- <link href="{{ asset('css/navbar.css') }}" rel="stylesheet"> --}}
    {{-- <link href="{{ asset('css/reset.css') }}" rel="stylesheet"> --}}
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <link rel="stylesheet" href="{{ url('dmenu/css/reset.css') }}"> <!-- CSS reset -->
    <link rel="stylesheet" href="{{ url('dmenu/css/style.css') }}">
     <!-- Resource style -->
    <script src="{{ url('dmenu/js/modernizr.js') }}"></script> <!-- Modernizr -->

</head>
<body>
    <div id="app">
       @include('layouts.navbar')
        <div class="conatiner">
            <div class="row">
                   <div class=" col8PaddingLeft col-md-9">
                        <div class="panel panel-primary">
                        <div class="panel-heading">@yield('panel-heading')</div>
                            <div class="panel-body">
                                @include('layouts.messages')
                                @yield('content')
                            </div>
                        </div>
                   </div>

                    @include('layouts.sidebar')


                </div>
            </div>
</div>



        <script type="text/javascript">
    $.ajaxSetup({
      headers : {
        'X-CSRF-TOKEN' : $('meta[name="csrf_token"]').attr('content')
      }
    })
    $('#login-form').submit(function(event){
      event.preventDefault()
      var postData = {
        'email' : $('input[name=email]').val,
        'password' : $('input[name=password]').val,
        //'remember_me' : $('input[name=remember_me]').is(':checked'),
      }
      $.ajax({
        type:'POST',
        url : '/login',
        data: postData
      })
    });
  </script>

    <!-- Scripts -->
{{--     <script src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="  crossorigin="anonymous"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
    <script src="{{ asset('js/jquery-menu-aim.js') }}"></script> --}}

      <script src="{{ asset('user/js/site.js') }}"></script>
      <script src="{{ asset('user/js/site2.js') }}"></script>

<script src="{{ url('dmenu/js/jquery.menu-aim.js') }}"></script> <!-- menu aim -->
<script src="{{ url('dmenu/js/main.js') }}"></script> <!-- Resource jQuery -->

</body>
</html>
