<div class=" col-md-3 pull-right">

@if(isset($categories))
  <h2 class="side-bar__title">Categories</h2>
      <ul class="list-categories list-unstyled">
        @foreach($categories as $category)
          <li class="list-categories__item list-item">
              <a class="list-categories__link" href="/subject/{{ $category->subject }}/{{ $category->category }}">
                 {{ $category->category }}
              </a>
          </li>
         @endforeach
    </ul>
 @endif
 @if(isset($online_users) )
  <h2 class="side-bar__title">Online Users ({{count($online_users)}})</h2>
      <ul class="list-categories list-unstyled">
        @foreach($online_users as $user)
          @if ($user->id === \Sentinel::getUser()->id)
            <li class="list-categories__item list-item">
              <a class="list-categories__link"  href="/profile/{{$user->username}}">
                  <span ><i class="fa fa-user"></i></span> Just You<small>({{ $user->roles->first()->name }})</small>
              <span ><i class="fa fa-circle" style="color:lightgreen"></i></span>
              </a>
{{--               <div class="pull-right">
              <span ><i class="fa fa-circle" style="color:lightgreen"></i></span>
              </div> --}}

          </li>
          @continue
          @endif
          <li class="list-categories__item list-item">
              <a class="list-categories__link"  href="/profile/{{$user->username}}">
                  <span ><i class="fa fa-user"></i></span> {{ $user->first_name . ' ' . $user->last_name }} <small>({{ $user->roles->first()->name }})</small>
              </a>
              <div class="pull-right">
              <span ><i class="fa fa-circle" style="color:lightgreen"></i></span>
              </div>

          </li>
         @endforeach
    </ul>

  @endif
  @if(isset($archives))
    <h2 class="side-bar__title">Archives</h2>
      <ul class="list-categories list-unstyled">
        @foreach($archives as $stats)
          <li class="list-categories__item list-item">
              <a class="list-categories__link" href="{{ route('archives',['month' => $stats['month'],'year' => $stats['year']]) }}">
                  {{ $stats['month'] . ' ' .$stats['year'] }}
              </a>
          </li>
         @endforeach
    </ul>

  @endif
  @if(isset($tags) )
  <h2 class="side-bar__title">Popular Tags</h2>
      <ul class="list-categories list-unstyled">
        @foreach($tags as $tag)
          <li class="list-categories__item list-item">
              <a class="list-categories__link"  href="/tags/{{ $tag->name }}">
                  {{ $tag->name }}
              </a>
          </li>
         @endforeach
    </ul>

  @endif
  @if(isset($posts))
  <h2 class="side-bar__title">Recent Posts</h2>
      <ul class="list-categories list-unstyled">
        @foreach($posts as $post)
          <li class="list-categories__item list-item">
              <a class="list-categories__link"  href="/posts/{{ $post->title }}">
                  {{ $post->title }}
              </a>
          </li>
         @endforeach
    </ul>
  @endif
  @if(isset($popularPosts))
  <h2 class="side-bar__title">Popular Posts</h2>
      <ul class="list-categories list-unstyled">
        @foreach($popularPosts as $post)
          <li class="list-categories__item list-item">
              <a class="list-categories__link"  href="/posts/{{ $post->title }}">
                  {{ $post->title }}
              </a>
          </li>
         @endforeach
    </ul>
  @endif
  @if(isset($lr_tips))
 <ul class="list-group">
  @foreach($lr_tips as $tip)
    <li class="list-group-item"><span class="fa fa-check-square fa-lg alert-success"></span> {{ $tip }}</li>

  @endforeach
  <li class="list-group-item">  <p class="text-center"><a  class="btn btn-primary" href="{{ route('register') }}"> Register now </a> </p></li>
 </ul>
 @endif
 <h2 class="side-bar__title">Follow us</h2>
            <ul class="social-links list-inline">
                <li><a href="https://www.facebook.com/">
                <img src="{{ asset('user/svg/icon-facebook.svg') }}" width="32=&quot;true&quot;" height="32=&quot;true&quot;" alt="icons/icon-facebook.svg"></a></li>
                 <li><a href="https://twitter.com/">
                <img src="{{ asset('user/svg/icon-twitter.svg') }}" width="32=&quot;true&quot;" height="32=&quot;true&quot;" alt="icons/icon-twitter.svg"></a></li>
                <li><a href="https://www.youtube.com/user/">
                <img src="{{ asset('user/svg/icon-youtube.svg') }}" width="32=&quot;true&quot;" height="32=&quot;true&quot;" alt="icons/icon-youtube.svg"></a></li>
            </ul>
 </div>

