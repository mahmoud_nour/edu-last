<header>



{{-- Start menu --}}
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-left">
                     <li><a href="/">home</a></li>
                     @if( Sentinel::getUser('admin','moderator','user'))
                    @endif
                    <li><a href="{{ url('/posts') }}">posts</a></li> <!--  drop menu-->

                    <li>

        <div class="cd-dropdown-wrapper">
            <a class="cd-dropdown-trigger" href="#0">Dropdown</a>
            <nav class="cd-dropdown">
                <h2>Title</h2>
                <a href="#0" class="cd-close">Close</a>
                <ul class="cd-dropdown-content">
                    <li class="has-children">
                        <a href="http://codyhouse.co/?p=748" class="dropdown-is-active f-list ">Clothing</a>
                        <ul class="cd-secondary-dropdown is-hidden ">
                            <li class="go-back"><a href="#0">Menu</a></li>
                            <li class="has-children">
                                <a href="http://codyhouse.co/?p=748">Accessories</a>

                                <ul class="is-hidden">
                                    @foreach ($menus as $branchName => $menu)
                                        <li class="go-back"><a href="#0">{{  $branchName }} </a></li>

                                         <li class="has-children">
                                        <a href="#0"> {{  ucfirst(str_replace(['-','_'],' ', $branchName)) }}</a>
                                        <ul class="is-hidden">
                                            <li class="go-back">
                                                <a href="#0">Accessories</a>
                                            </li>
                                            <li>
                                                <li class="has-children">
                                                    @foreach ($menu as $yearName => $submenu)
                                                        <a href="#0"> {{  $yearName }}</a>
                                                    <ul class="is-hidden">
                                                         <li class="go-back"><a href="#0">back</a></li>
                                                        <li >
                                                            @foreach ($submenu as $termName => $subjects)
                                                                {{-- expr --}}
                                                    <li class="has-children">
                                                            <a href="#0">{{  ucfirst(str_replace(['_','-'],' ',$termName)) }}</a>
                                                    <ul class="is-hidden">

                                                        <li class="go-back"><a href="#0">back</a></li>
                                                        @foreach ($subjects as $subject)
                                                            @foreach (explode(',',$subject->first()->subjects) as $subjectName)
                                                                    <li><a href="{{ route('posts.category.index',$subjectName) }}"> {{  $subjectName }} </a></li>
                                                             @endforeach
                                                        @endforeach
                                                    </ul>
                                                </li>
                                                            @endforeach
                                                         </li>
                                                    </ul>
                                                    @endforeach

                                                </li>
                                             </li>
                                        </ul>
                                    </li>
                                    @endforeach
                                    {{-- <li class="see-all"><a href="http://codyhouse.co/?p=748">All Accessories</a></li> --}}


                    <li><a href="http://codyhouse.co/?p=748">Page 3</a></li>
                </ul> <!-- .cd-dropdown-content -->

            </nav> <!-- .cd-dropdown -->
        </div> <!-- .cd-dropdown-wrapper -->
        </li>
    </ul>
        {{-- end -drop menu --}}


                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if(Sentinel::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    @if (Sentinel::getUser()->profile_picture)
                                        @php
                                            $profile_picture = Sentinel::getUser()->profile_picture;
                                        @endphp
                                        <img src="{{ asset("$profile_picture") }}" style="max-width: 30px;max-height: 50px;border-radius: 50%">
                                    @else
                                    <span><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i></span>
                                    @endif
                                    <span style="position: relative;top: -10px;">{{ Sentinel::getUser()->first_name }} <span class="caret"></span>
                                    </span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li> <a href="{{ route('2fa-settings') }}">
                                           Authentication Settings
                                        </a></li>
                                </ul>
                            </li>
                            {{-- Notification Here --}}
                            <notification :userid="{{Sentinel::getUser()->id }}" :unreads="{{ Sentinel::getUser()->unreadNotifications }}"></notification>
                        @endif
                    </ul><!-- Right Side Of Navbar -->
    </div>

</header>


