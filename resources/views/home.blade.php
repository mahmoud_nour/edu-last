@extends('layouts.app')
{{-- @include('layouts.navbar') --}}

<style>
  body {
    overflow-x:hidden;
}
</style>
@section('container-row')
  @if(count($RecentPosts) > 0)
    @foreach ($RecentPosts as $subject => $RecentPostsForSubject)
        @foreach ($RecentPostsForSubject as $posts)
      <div class="col-md-9">
        <div class=" row"> {{-- row --}}
          @foreach ($posts as $post)
            <div class="col-md-12 ">
              <header class="category__header">


                <h2 class="category__title">recent posts for {{ $subject }}</h2>

                <a class="category__link link--black" href="{{ route('posts.category.index',$post->subject) }}">View all </a>

              </header>
            </div>
          @break
          @endforeach
          @foreach ($posts as $post)

            <div class="col-6 col-lg-4">
            <a href="{{ route('posts.show',$post->title) }}"><h2>{{  $post->title }}</h2>
              <div class="card__image">
                <img src="{{ asset("images/$post->imagePath") }}" alt="{{ $post->title }}" class=" img-responsive">
              </div>
            </a>
              <a href="#"><h4>{{  str_limit($post->body,50) }} </h4></a>
              <p><a class="btn btn-secondary" href="{{ route('posts.show',$post->title) }}" role="button">View details »</a></p>
            </div><!--/span-->
              @endforeach
              </div>{{-- row --}}

        </div> <!--  col-md-9-->
      @endforeach
    @endforeach

  @endif
@endsection
