@extends('layouts.master')
 
 <style >
 .glyphicon {
    line-height: 1.5 !important;
 }
 .btn-xs, .btn-group-xs > .btn {
    padding: 1px 5px;
    font-size: 12px;
    line-height: 1.5 !important;
    border-radius: 3px;
}
.table{
font-family: sans-serif !important;
font-size: 14px;
}
 </style>
 @section('content')
 {{--start- table --}} 
 <?php $posts = \App\Post::all()  ?>

<div class="row table"> 
        <div class="col-md-12">
        <h4>Bootstrap Snipp for Datatable</h4>
        <div class="table-responsive"> 
              <table id="mytable" class="table table-bordred table-striped">
	<thead>
		{{-- <th><input type="checkbox" id="checkall" /></th> --}}
		<th>ID</th>
		<th>title</th>
		<th>body</th>
		<th>department</th> 
		<th>approved</th>
		<th>Delete</th>
	</thead>
    <tbody>
     @foreach ($posts as  $post)
    <tr>
    {{-- <td><input type="checkbox" class="checkthis" /></td> --}}
    <td>{{ $post->id }}</td>
    <td> {{ str_limit($post->title, 10) }}</td>
    <td> {{ str_limit($post->body, 20) }}</td> 
    <td> {{ str_limit(str_replace("_", " ", $post->department), 20) }}</td>  
 {!! $post->approved ==0 ? " <td><span class=\"label label-warning\">Pending</span></td> " : "<td> <span class=\"label label-success\">approved</span>
</td>"  !!}

    <td>
	    <p data-placement="top" data-toggle="tooltip" title="approved">
        	    <button class="btn btn-primary btn-xs" data-title="approved" data-toggle="modal" data-target="#approved" >
        	       <span class="glyphicon glyphicon-pencil"></span>
        	    </button>
      </p>
    </td>
    <td>
        <p data-placement="top" data-toggle="tooltip" title="Delete">
            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" >
              <span class="glyphicon glyphicon-trash"></span>
            </button>
        </p>
    </td>
    </tr>
 @endforeach
 
       </tbody>
        
</table>

<div class="clearfix"></div>
<ul class="pagination pull-right">
  <li class="disabled">
  	<a href="#">
  	<span class="glyphicon glyphicon-chevron-left"></span>
  	</a>
</li>
  <li class="active"><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
</ul> 
            </div>
        </div>
</div> 

<div class="modal fade" id="approved" tabindex="-1" role="dialog" aria-labelledby="approved" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">approved Your Detail</h4>


      </div>
          <div class="modal-footer ">
            {{--  --}}
            <a  class="dropdown-item" href="{{ route('posts.approve',$post->id) }}" onclick="event.preventDefault(); document.getElementById('form-control').submit();">
                   <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span>approved</button>
            </a>
            <form id="form-control" action="{{ route('posts.approve',$post->id) }}" method="POST" style="display: none;">
               {{ csrf_field() }}
            </form>
            {{--  --}}
        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>un approve</button>
          <a  href="{{ route('posts.edit',$post->title) }}"> <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-remove"></span>Edit</button></a>

       </div>
        </div>

    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div> 

    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="approved" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
      </div>
          <div class="modal-body">

       <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>
       
      </div>
        <div class="modal-footer ">
 {{--  --}}
  <a  class="dropdown-item" href="{{ route('posts.destroy',$post->title) }}" onclick="event.preventDefault(); document.getElementById('destroy-form').submit();">
        <button type="button" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span>Yes</button>
  </a>
  <form id="destroy-form" action="{{ route('posts.destroy',$post->title) }}" method="POST" style="display: none;">
     {{ csrf_field() }}
     {{ method_field('DELETE')}}
  </form>
 {{--  --}} 
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div> 
{{--end table --}} 
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script >
$(document).ready(function(){$("#mytable #checkall").click(function () {
        if ($("#mytable #checkall").is(':checked')) {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });
        } else {
            $("#mytable input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("[data-toggle=tooltip]").tooltip();
}); 

$(function(){
       $('#button').click(function() {
            $.ajax({
                url: 'admin/{id}',
                type: 'post',
                data: { id: 1 },
                success: function(response)
                {
                
                  alert('response');
                }
            });
       });
    });   
</script>