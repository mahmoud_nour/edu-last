@extends('layouts.app')
@section('panel-heading')
    <p class="text-center">All Posts</p>
@endsection
@section('content')
<a href="{{ url('posts/create') }}">create</a>
    @if (count($posts) > 0)
        @foreach ($posts as $post)
          <h4 class="text-center">
            <strong>
              <a href="/posts/{{ $post->title }}">{{ $post->title }}</a>
              </strong>
          </h4>
        <a href="#" class="thumbnail">
            @php
                if(is_null($post->imagePath)){
                    $post_image = 'http://placehold.it/260x180';
                }else{
                    $post_image = 'images/' . $post->imagePath;
                }
            @endphp
            <img src="{{ $post_image }} " alt="">
        </a>
  
         <p class="text-center ">
          {{ str_limit($post->body,50) }}
        </p>
        <p>
            <a href="/posts/{{ $post->title }}"><small>Show more</small></a>
        </p>
         <p>
         <small> 
         <blockquote class="blockquote">

 <span class="label label-primary"> {{  $post->likes->count() }} {{ str_plural('like', $post->likes->count()) }}</span>

   @foreach ($post->likes as $userLiked)
      <span class="label label-info">{{$userLiked->user->username}}</span>
   @endforeach
            <i class="icon-user "></i> by <a href="#">{{ $post->admin->first_name }}</a> 
            | <i class="icon-calendar"></i> {{ $post->created_at}}
            | <i class="icon-comment"></i> <a href="#">{{ $post->comments->count() }} Comments</a>
            @if ($post->tags->count())
            | <i class="icon-tags"></i> Tags : 
            @foreach ($post->tags as $tag)<a href="/tags/{{$tag->name }}"><span class="label label-info">{{ $tag->name}}</span></a>
         </small>
          </blockquote>
            @endforeach
            @endif
        </p>
<hr> 
        @endforeach

    @else
        <div class="jumbotron">
           There is no posts to show  
        </div>
    @endif
@endsection