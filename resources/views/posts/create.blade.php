@extends('layouts.app')

@section('panel-heading')
    Create A Post
@endsection
@section('content')
  <form action="{{ route('posts.store') }}" method="POST" enctype='multipart/form-data'>
        {{csrf_field()}}
        <div class="form-group">
            <label for="title"> Title</label>
            <input type="text" name="title" class="form-control" placeholder="title" value="{{ old('title')}}">
        </div>
        <div class="form-group">
            <label for="body">Post Body</label>
            <textarea name="body" id="article-ckeditor" class="form-control" placeholder="Body Text"> {{ old('body')}}</textarea>
        </div>
        <div class="form-group">
              <label for="department"> Department</label>
            <select class="form-control" name="department">
              <option selected disabled>Pick Up A Department</option>
                <option value="electronics_and_communication">Electronics & Communication</option>
              <option value="civil">Civil</option>
              <option value="electrical_power">Electrial Power</option>
              <option value="bio_medical">Bio Medical</option>
              <option value="chemical"> Chemical </option>
              <option value="architecture">Architecture</option>
            </select>
        </div>
      
    
        <div class="form-group">
        <label for="year">Education Year</label>
            <select class="form-control" name="edu_year">
              <option selected disabled>Pick Up A Year</option>
                <option value="Preparatory_Year">Preparatory Year</option>
                <option value="First_Year">First Year</option>
              <option value="Second_Year">Second Year</option>
              <option value="Third_Year">Third Year</option>
              <option value="Fourth">Fourth Year</option>
            </select>
        </div>
        <div class="form-group">
            <label for="image"> Upload an Image For Post</label>
            <input type="file" name="imagePath">
        </div>
         <div class="form-group">
            <label for="pdf">Upload a PDF for post</label>
            <input type="file" name="pdfPath">
        </div>
          <div class="form-group">
            <label for="category">Category</label>
            <select class="form-control" name="category">
              <option selected disabled>Pick Up A Category</option>
              <option value="section">Section</option>
              <option value="lecture">Lecture</option>
              <option value="final">Final Exam</option>
              <option value="midterm">Mid Term Exam</option>
            </select>
        </div>
         <div class="form-group">
            <label for="category">subject</label>
            <select class="form-control" name="subject">
              <option selected disabled>Pick Up A Category</option>
              <option value="math2">Math(2)</option>
              <option value="electronics2">electronics</option>
            </select>
        </div>
        <div class="form-group">
            <label for="term">Term</label>
            <select class="form-control" name="term">
              <option selected disabled>Pick Up A Term</option>
              <option value="first_term">First Term</option>
              <option value="second_term">Second Term</option>
            </select>
        </div>
        <div class="form-group">
            <label for="tags">Tag</label>
            <input type="text" name="tags" class="form-control">
        </div>

        <div class="form-group">
            <input type="submit" value="Release Post" class="btn btn-primary">
        </div>
  </form>
 

@endsection