@extends('layouts.app')

@section('panel-heading')
    Edit A Post
@endsection
@section('content')
  <form action="{{  route('posts.update',$post->title) }}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
         <div class="form-group">
            <label for="title"> Title</label>
            <input type="text" name="title" class="form-control" placeholder="title" value="{{ $post->title }}">
        </div>
        <div class="form-group">
            <label for="body">Post Body</label>
            <textarea name="body" id="article-ckeditor" class="form-control" placeholder="Body Text"> {{ $post->body }}</textarea>
        </div>
        
        <div class="form-group">
              <label for="department"> Department</label>
            <select class="form-control" name="department">        
              <option selected disabled>Pick Up A Department</option>
                <option value="electronics_and_communication" {{ ($post->department === "electronics_and_communication") ? "selected" : ''}}>Electronics & Communication</option>
              <option value="civil" {{ ($post->department === "civil") ? "selected" : ''}} >Civil</option>
              <option value="electrical_power" {{ ( $post->department === "electrical_power") ? "selected" : ''}}>Electrial Power</option>
              <option value="bio_medical" {{ ( $post->department === "bio_medical") ? "selected" : ''}} >Bio Medical</option>
              <option value="chemistry" {{ ($post->department === "chemistry") ? "selected" : ''}} > Chemistry </option>
              <option value="architecture" {{ ( $post->department === "architecture") ? "selected" : ''}} >Architecture</option>
            </select>
        </div>
 
        <div class="form-group">
        <label for="year">Education Year</label> 
            <select class="form-control" name="edu_year">
              <option selected disabled>Pick Up A Year</option>
                <option value="Preparatory_Year" {{ ( $post->edu_year === "Preparatory_Year") ? "selected" : ''}}>Preparatory Year</option>
                <option value="First_Year" {{ (  $post->edu_year === "First_Year") ? "selected" : ''}}>First Year</option>
              <option value="Second_Year" {{ ( $post->edu_year === "Second_Year") ? "selected" : ''}}>Second Year</option>
              <option value="Third_Year" {{ (  $post->edu_year === "Third_Year") ? "selected" : ''}}>Third Year</option>
              <option value="Fourth_Year" {{ (  $post->edu_year === "Fourth_Year") ? "selected" : ''}}>Forth Year</option>
            </select>
        </div>

        <div class="form-group">
            <label for="image"> Upload an Image For Post</label>
            <input type="file" name="imagePath">
        </div>

         <div class="form-group">
            <label for="pdf">Upload a PDF for post</label>
            <input type="file" name="pdfPath">
        </div>

          <div class="form-group">
            <label for="category">Category</label>
            <select class="form-control" name="category">
              <option selected disabled>Pick Up A Category</option>
              <option value="section" {{ $post->category === "section" ? "selected" :""  }}>Section</option>
              <option value="lecture" {{ $post->category === "lecture" ? "selected" :""  }}>Lecture</option>
              <option value="final" {{ $post->category === "final" ? "selected" :""  }}>Final Exam</option>
              <option value="mid-term" {{ $post->category === "mid-term" ? "selected" :""  }}>Mid Term Exam</option>
             </select>
        </div>
          @php
            $hint = $post->hints()->where('term',$post->term)->first();
          @endphp
         <div class="form-group">
            <label for="category">subject</label>

              
            <select class="form-control" name="subject">
              <option selected disabled>Pick Up A Subject</option>
              {{-- <option value="Math2">Math(2)</option> --}}
              @foreach (explode(',',$hint->subjects) as $subject)
                 <option value="{{ $subject }}" {{$post->subject  === $subject ? "selected" : ''}}>{{  $subject }}</option>
              @endforeach
            </select>
        </div> 

        <div class="form-group">
            <label for="term">Term</label>
            <select class="form-control" name="term">
              <option selected disabled>Pick Up A Term</option>
                      <option value="first_term" {{$post->term  === "first_term" ? "selected" : ''}}>first term</option>
                      <option value="second_term" {{$post->term  === "second_term" ? "selected" : ''}}>second term</option>
            </select>
        </div>
        <div class="form-group">
            <label for="tags">Tag</label>
            <input type="text" name="tags" class="form-control" value="{{ implode(' ', $post->tags->pluck('name')->toArray()) }}">
        </div>

        <div class="form-group">
            <input type="hidden" name="_method" value="PUT">
            <input type="submit" name="submit" value="Release Post" class="btn btn-primary">
        </div>
  </form>
  
@endsection