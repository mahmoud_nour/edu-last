@extends('layouts.app')
@section('style')
 
<link href="{{ asset('css/comment.css') }}" rel="stylesheet">

@endsection

<Style>
  #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > div.modal.fade.bs-example-modal-sm.in > div > div > ul > ol{
    padding: 0;

  }
    #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > div.modal.fade.bs-example-modal-sm.in > div > div{
      margin-top: 18em;
    }
    #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > div.modal.fade.bs-example-modal-sm.in > div{
      margin-left: 14em;
    }
    .modal-dialog{
    }
    #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > div.modal.fade.bs-example-modal-sm.in > div > div{
      border: 0;
    }
    #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > div.modal.fade.bs-example-modal-sm.in > div > div > ul{
      
    padding-left: 9px;
    padding-top: 5px;
    padding-bottom: 5px;

    }
    #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > ul{
      margin-left: 0;
    }
 /*   #app > div > div > div.col-md-9 > div > div.panel-body > div.row > div.post-description > ul > li > p:nth-child(1) > small > a{
      width: 50px;
      height: 36px;
    }
    #app > div > div > div.col-md-9 > div > div.panel-body > div > div.post-description > ul > li > p:nth-child(1) > small > a > i{
      margin-top: 3px;
    }*/
    .reply {
        background-color: rgba(245, 245, 245, 0.66);
        box-shadow:  inset  1px 1px 1px rgba(136, 136, 136, 0.33),1px 1px 1px rgba(136, 136, 136, 0.33);
        padding: 9px 6px 1px;
    }
</Style>
@section('panel-heading')
<p class="text-center">{{ $post->admin->first_name . ' ' . $post->admin->last_name . '\'s '}} Post</p>
@endsection

@section('content')

  <div class="row">

      <div class="panel panel-white post panel-shadow" style="background-color: rgba(221, 221, 222, 0.24);">
            <div class="post-heading">
               <div class="pull-left image"> 
                  <img src=" {{ asset('profile_pictures/'. ($post->admin->profile_picture  ?? 'default.png')) }}" class="img-circle avatar" alt="user profile image" style="max-width: 50px;max-height: 50px;border-radius: 50%">
               </div>
               <div class="pull-left meta">
                  <div class="title h5">
                     <a href="#"><b>{{ $post->admin->first_name . ' ' . $post->admin->last_name }}</b></a>
                     created a post
                     <p> <small>{{ $post->admin->roles->first()->slug }}</small></p>

                     @if(Sentinel::check() && Sentinel::getUser()->hasAnyAccess(['admin.*']))
                     @if ($post->approved_by != null)
                       <p> <small>approved_by <strong>
                         {{-- approved_by != null --}}
                        <a href="/profile/{{$post->approved_by}}">{{ $post->approved_by}}</a>
                       </strong></small></p>
                     @endif
                     
                     @endif
                  </div>
                  <h6 class="text-muted time"> {{ $post->created_at }}</h6>
               </div>
            </div>
<div class="clearfix"></div>
      <div class=" panel panel-white post panel-shadow post-description"> {{-- post-description --}}

         <p> 
          {{ $post->body }}
         </p>
      </div> 
@if($post->pdfPath)
    <form action=" {{ route('posts.download',$post->title) }}" method="POST">
          {{ csrf_field() }}
      <input type="submit"  value="download" class="btn fa btn-default"> 
    </form>
@endif

<!-- Small modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content mod">
<ul>
     @foreach ($post->likes as $userLiked)
        <ol>
            <a href="/profile/{{ $userLiked->user->username }}" style="text-decoration: none">
                <img src=" {{ asset('profile_pictures/'. ($userLiked->user->profile_picture  ?? 'default.png')) }}" class="img-circle avatar" alt="user profile image" style="max-width: 30px;max-height: 30px;border-radius: 50%">
                 {{  $userLiked->user->first_name . ' ' . $userLiked->user->last_name }}
             </a>
           <small>{{ $userLiked->created_at}}  </small>
        </ol>
    @endforeach
</ul>
    </div>
  </div>
</div>
<!-- -->          
{{-- likes modal --}}
<ul class="list-inline">
  @if(\Sentinel::check())
      @if (\Sentinel::getUser()->id !== $post->admin_id) 
               
                     <li> 
                           <p><small>
                                 <a href="{{ route('dislike',['type' => 'Post','id' => $post->id ]) }}" onclick="event.preventDefault(); document.getElementById('dislike-form').submit();" class="btn btn-default stat-item">
                                  unlike <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> 
                                 </a>
                                   <form id="dislike-form" action="{{ route('dislike',['type' => 'Post','id' => $post->id ]) }}" method="POST" style="display: none;">
                                         {{ csrf_field() }}
                                   </form>
                                 </small>
                              </p>
                        </li> 
            
                        <li>

                <p><small>
                <a href="{{ route('like',['type' => 'Post','id' => $post->id ]) }}" onclick="event.preventDefault(); document.getElementById('like-form').submit();" class="btn btn-default stat-item">
                {{-- {{  $post->likes->count() }} {{ str_plural('like', $post->likes->count()) }}   --}} 
                    Like  <i class="fa fa-thumbs-o-up icon"></i> </a>

                <form id="like-form" action="{{ route('like',['type' => 'Post','id' => $post->id ]) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                </form>

                </small></p> 
                        </li>
@endif
               

          @endif
          <li>

      @if(\Sentinel::check())
            @if($like->status('Post',$post->id) !== null)

                      <p>
                        <small>
                            <a href="{{ route('reset-like',['type' => 'Post','id' => $post->id ]) }}" onclick="event.preventDefault(); document.getElementById('reset-like-form').submit();" class="btn btn-default stat-item">
          {{-- {{  $post->likes->count() }} {{ str_plural('like', $post->likes->count()) }}   --}} 
                          Reset Like/Unlike<i class="fa fa-back"></i> </a>

                            <form id="reset-like-form" action="{{ route('reset-like',['type' => 'Post','id' => $post->id ]) }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                            </form>
                      </small>
          </p>          

          @endif
          @endif
          </li>
{{--           @if(Sentinel::check())
          <a href="#" onclick="window.location.hash='#comment'" class="btn btn-default stat-item">
                comment
               <i class="fa fa-commenting-o"></i>
          </a>
          @endif --}}
          <br>
          {{-- likes counter --}}
<a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">{{  $like->counter($post,1) }} {{-- {{ str_plural('like', $post->likes->count()) }}  --}}<i class="fa fa-thumbs-up icon"></i>  
 </a>
 <a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">{{  $like->counter($post,0) }} {{-- {{ str_plural('like', $post->likes->count()) }}  --}}<i class="fa fa-thumbs-down icon"></i>  
 </a>
          {{-- likes counter --}}
          {{-- commnet counter --}}
          <a href="#" class=""> 
          {{  $post->comments()->where('approved',1)->count() + $post->replies()->where('approved',1)->count() }} 
          <i class="fa fa-comment-o" aria-hidden="true"></i>
          </a>
          {{-- commnet counter --}}
</ul>
{{-- likes --}}

@if(Sentinel::check())
@if(Sentinel::getUser()->hasAnyAccess(['*.edit']))
@if(Sentinel::getUser()->id === $post->admin_id || Sentinel::getUser()->hasAccess('admin.*'))
            <div class="pull-right">{{-- pull-right--}}     
                  <ul class="nav  navbar-default">{{-- nav  navbar-default--}}
                       <li class="dropdown"> {{-- li-dropdown --}}
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" >
                          Post Managing <span class="caret"></span>
                      </a>

                            <ul class="dropdown-menu" role="menu">
                                @if ($post->approved === 0)
                                @if (Sentinel::getUser()->hasAccess('admin.approve'))
                            <li>
                              <a href="{{ route('posts.approve',$post->id) }}" onclick="event.preventDefault(); document.getElementById('approve-form').submit();">
                                Approve
                              </a>
                              <form id="approve-form" action="{{ route('posts.approve',$post->id) }}" method="POST" style="display: none;">
                                 {{ csrf_field() }}
                              </form>
                            </li>
@endif
@endif

@if ( Sentinel::check() && Sentinel::getUser()->hasAnyAccess(['*.delete']))
                         <li>
                              <a  class="dropdown-item" href="{{ route('posts.destroy',$post->title) }}" onclick="event.preventDefault(); document.getElementById('destroy-form').submit();">
                              Delete 
                              </a>
                              <form id="destroy-form" action="{{ route('posts.destroy',$post->title) }}" method="POST" style="display: none;">
                                 {{ csrf_field() }}
                                 {{ method_field('DELETE')}}
                              </form>
                          </li>
@endif

@if ( Sentinel::check() && Sentinel::getUser()->hasAnyAccess(['*.edit']))
<li> 
      <a class="dropdown-item" href="{{ route('posts.edit',$post->title) }}">Edit</a>
</li>

@endif
                            </ul>
                       </li> {{-- li-dropdown --}}
                  </ul>{{-- nav  navbar-default--}}

            </div> {{-- pull-right--}}

@endif
@endif
@endif
      </div> {{-- post-description --}} 
      <div class="post-footer">
         <form action="{{ route('comments.store',$post->title) }}" method="POST">
            {{ csrf_field() }}
            <div class="input-group"> 
               <span class="input-group-addon">
               <a href="#"><i class="fa fa-pencil"></i></a>  
               </span>
               <input class="form-control" placeholder="Add a comment" type="text" style="height: 50px;" name="comment" id="comment">
            </div>
         </form>

@foreach ($post->comments as $comment)
@if ($comment->approved === 1 || (Sentinel::check() && Sentinel::getUser()->hasAnyAccess(['*.approve'])))
                  

                      <a class="pull-left" href="/profile/{{$comment->user->username}}">
                      <img src="{{ asset('profile_pictures/'. ($comment->user->profile_picture  ?? 'default.png')) }} " class="img-circle avatar" alt="user profile image" style="max-width: 50px;max-height: 50px;border-radius: 50%">
                      </a>
                {{-- comment-body --}}
               <div class="comment-body " style="background-color: rgba(234, 234, 234, 0.49)">
                  <div class="comment-heading">{{-- comment-heading --}}
                     {{-- <h4 class="user">{{ $comment->user->first_name . ' ' . $comment->user->last_name }}</h4>11  --}}
<a href="/profile/{{$comment->user->username}}">{{ $comment->user->first_name . ' ' . $comment->user->last_name}} </a>
<p>
  <small class="time">
    @if (isset($comment->updated_at))
    {{ 'Edited ' . $comment->updated_at}}
    @if ($comment->user_id != $comment->edited_by && $comment->edited_by && edited_body != null)
    <a href="/profile/{{$comment->editor->username}}">{{ $comment->editor->first_name . ' ' . $comment->editor->last_name}} </a>
    @endif
    @else
    {{ $comment->created_at}}
    @endif
</small>
</p>
                    
                      {{-- <p><small>{{ $comment->user->roles->first()->username}}</small></p> --}}
                  </div>{{-- comment-heading --}}

@if (\Route::currentRouteName() === 'comments.edit')
@if (request()->route('comment')->id === $comment->id)
                  <form id="comment-update-form" action="{{ route('comments.update',['comment' => $comment->id , 'post' => $post->title]) }}" method="POST">
                     <input type="text" name="comment" value="{{ $comment->body }}" class="form-control">
                     <hr>
                     <input type="submit" name="form-control" value="Edit Comment" class="form-control">
                     {{ csrf_field() }}
                     {{ method_field('PUT')}}
                  </form>
@endif
@else
  @if(!is_null($comment->edited_body))
                  <h4 class="">{{ $comment->edited_body }}</h4>
  @else
                  <h4 class="">{{ $comment->body }}</h4>
  @endif
@endif
                  <small>
                     <a href="/comments/{{$comment->id}}/{{$post->title}}">Edit</a>
                     |
                     <a href="{{ route('comments.destroy',$comment->id) }}"
                        onclick="event.preventDefault();
                        document.getElementById('destroy-comment-form').submit();">
                     Delete
                     </a>
                     <form id="destroy-comment-form" action="{{ route('comments.destroy',$comment->id) }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        {{ method_field('DELETE')}}
                     </form>

@if ($comment->approved === 0) 
                      |
                     <a href="{{ route('comments.approve',$comment->id) }}" onclick="event.preventDefault();document.getElementById('approve-comment-form').submit();">Approve
                     </a>
                     <form id="approve-comment-form" action="{{ route('comments.approve',$comment->id) }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}
                     </form>
                     {{-- expr --}}
@endif
                  </small>
               </div>{{-- comment-body --}}

               <ul class="comments-list">
                   
                  {{--    <form action="{{ route('replies.store',['post' => $post->title , 'comment' => $comment->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group"> 
                           <span class="input-group-addon">
                           <a href="#"><i class="fa fa-pencil"></i></a>  
                           </span>
                           <input class="form-control" placeholder="Add a reply to  comment" type="text" style="height: 50px;" name="comment">
                        </div> 



                        <div class="form-group">
                          
                        </div>
                     </form> --}}
<hr>
@if ($comment->replies->count())
@foreach ($comment->replies as $reply)
@if ($reply->approved === 1 || (Sentinel::check() && Sentinel::getUser()->hasAnyAccess(['*.approve'])))
 
                     <a class="pull-left" href="/profile/{{$comment->user->username}}">
                     <img src="{{ asset('profile_pictures/'. ($comment->user->profile_picture  ?? 'default.png')) }}" class="img-circle avatar" alt="user profile image" style="max-width: 50px;max-height: 50px;border-radius: 50%">
                     </a>
                    
                     <div class="comment-body" >{{-- comment-body --}}
                        <div class="comment-heading" style="background-color: rgba(234, 234, 234, 0.49)">
                           <h4 class="user"><a href="/profile/{{ $reply->user->username }}">{{ $reply->user->first_name .' ' . $reply->user->last_name }}</a></h4>
                            <small class="time">
                           <p><small>{{ $reply->user->roles->first()->name }}</small></p>
                          @if (isset($comment->updated_at))
                        {{ 'Edited ' . $comment->updated_at}}

                            {{-- @if ($comment->user_id != $comment->edited_by)
                            <a href="/profile/{{$comment->editor->username}}">{{ $comment->editor->first_name . ' ' . $comment->editor->last_name}} </a>
                            @endif --}}

                        @else
                        {{ $comment->created_at}}
                        @endif
                          </small>
                        </div>

@if (\Route::currentRouteName() === 'replies.edit')
@if (request()->route('reply')->id === $reply->id)
                          <form id="comment-update-form" action="{{ route('replies.update',['reply' => $reply->id ,'comment' => $comment->id , 'post' => $post->title]) }}" method="POST">
                             <input type="text" name="comment" value="{{ $reply->edited_body ?? $reply->body }}" class="form-control">
<hr>
                             <input type="submit" value="Edit Reply" class="form-control btn btn-primary">
                             {{ csrf_field() }}
                             {{ method_field('PUT')}}
                          </form>
@endif
@else
<div class=" reply">
  @if (is_null($reply->edited_body))
    <p> {{ $reply->body }}</p>
@else
    <p> {{$reply->edited_body}}</p>
@endif
</div>
@endif
                    <p><small>
                        <a href="/replies/{{$reply->id}}/{{$post->title}}">Edit</a> |
                        <a href="{{ route('replies.destroy',$reply->id) }}" onclick="event.preventDefault(); document.getElementById('destroy-reply-form').submit();">
                                Delete
                        </a>
                          @if($reply->approved == 0)
                         |<a href="{{ route('replies.approve',$reply->id) }}" onclick="event.preventDefault(); document.getElementById('approve-reply-form').submit();">
                                Approve
                        </a>
                         <form id="approve-reply-form" action="{{ route('replies.approve',$reply->id) }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                      {{ method_field('PUT')}}
                      </form>
                        @endif
                      <form id="destroy-reply-form" action="{{ route('replies.destroy',$reply->id) }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                      {{ method_field('DELETE')}}
                      </form>
                     
                      </small></p>
              </div>
{{-- <form action="{{ route('replies.store',['post' => $post->title , 'comment' => $comment->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group"> 
                           <span class="input-group-addon">
                           <a href="#"><i class="fa fa-pencil"></i></a>  
                           </span>
                           <input class="form-control" placeholder="Add a reply" type="text" style="height: 50px;" name="comment">
                        </div>
                     </form> --}}


                     {{-- comment-body --}}
                     {{-- expr --}}
  @endif
  @endforeach
  @endif
   <form action="{{ route('replies.store',['post' => $post->title , 'comment' => $comment->id]) }}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group"> 
                           <span class="input-group-addon">
                           <a href="#"><i class="fa fa-pencil"></i></a>  
                           </span>
                           <input class="form-control" placeholder="Add a reply to  comment" type="text" style="height: 50px;" name="comment">
                        </div> 



                        <div class="form-group">
                          
                        </div>
                     </form>                   
               </ul>         
@endif
@endforeach
      </div>

  </div>   {{-- row --}}
@endsection
<script type="text/javascript">
  $('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
</script>